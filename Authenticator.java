// Dieser Code wurde im Teamprojekt MyLifeApp - TodoListe benutzt.
package pack;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

class Authenticator {
    private List<User> users = new ArrayList<>();
    private static String credentialsFilename = "credentials.txt";
    private static String credentialsDelimiter = "-"; // Trennzeichen fÃ¼r die Logindaten in credentials.txt

    Authenticator() {
        loadUsersFromCredentialsFile();
    }

    boolean registerUser(String username, String password) {
        if(username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }
        for (User user : users) {
            if(user.getUsername().equals(username)) {
                return false;
            }
        }
        NewUser.mkUser(username);
        User newUser = new User(username, password);
        addUserToCredentialsFile(newUser);
        users.add(newUser);
        return true;
    }

    boolean login(String username, String password) {
        for (User user : users) {
            if(user.matches(username, password)){
                return true;
            }
        }
        return false;
    }

    private void loadUsersFromCredentialsFile(){
        if (!Files.exists(Path.of(credentialsFilename))) return; // credentials.txt gibt es noch nicht
        users.clear();
        List<String> lines = FileReader.getListFromFile(credentialsFilename);
        String username = "";
        String password = "";
        for (String line : lines) {
            if(line.isEmpty()) continue; //leere Zeilen ignorieren
            if(line.equals(credentialsDelimiter)) {
                byte[] decoded = Base64.getDecoder().decode(password);
                users.add(new User(username, new String(decoded, StandardCharsets.UTF_8)));
                username = "";
                password = "";
            } else {
                if(username.isEmpty()) username = line;
                else password = line;
            }
        }
    }

    private void addUserToCredentialsFile(User user){
        List<String> lines = new ArrayList<>();
        // wenn es credentials.txt schon gibt, dann Zeilen in die Variable "lines" laden
        if (Files.exists(Path.of(credentialsFilename))) lines = FileReader.getListFromFile(credentialsFilename);
        lines.add(user.getUsername());
        byte[] password = user.getPassword().getBytes(StandardCharsets.UTF_8);
        String encoded = Base64.getEncoder().encodeToString(password);
        lines.add(encoded);
        lines.add(credentialsDelimiter);
        //nach dem Schreiben in credentials.txt entsteht eine leere Zeile -> hier rausfiltern, damit diese sich nicht ansammeln
        lines = lines.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
        try {
            FileWrite.writeFile(credentialsFilename, lines);
        } catch(IOException e) {
            System.out.println("Error: konnte nicht in " + credentialsFilename + " die Userdaten schreiben");
        }
    }
}
