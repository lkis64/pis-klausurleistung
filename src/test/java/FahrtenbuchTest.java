package pack;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import java.util.Random;

import static org.junit.Assert.*;
import static pack.App.getJourneystmpPath;
import static pack.App.getTemplatestmpPath;
import static pack.Journey.*;
import static pack.StoreableHandler.loadStoreable;
import static pack.StoreableHandler.saveStoreable;

public class FahrtenbuchTest {

    // Funktion die eine ArrayList mit 100 randomStrings bis 20 Zeichen erstellt (Groß-und kleinbuchstaben und Zahlen)
    private ArrayList<String> randomStrings(){
        Random random = new Random();
        ArrayList<String> randomStrings = new ArrayList<>();
        int randomNumber;
        for(int j = 0 ; j<100 ; j++) {
            StringBuilder randomStr = new StringBuilder();
            for (int i = 0; i < random.nextInt(20) + 1; i++) {
                do{
                    randomNumber = (random.nextInt(57) + 48);
                } while (randomNumber > 90 && randomNumber <97 || randomNumber >57 && randomNumber < 65);
                randomStr.append((char) randomNumber);
            }
            randomStrings.add(randomStr.toString());
        }
        return randomStrings;
    }

    private final ArrayList<String> teststrings = randomStrings();

    @Test
    public void encoderTest(){
        Coder coderForEncoderTest = new Coder();
        // Testen, ob der encoder den String tatsächlich kodiert
        teststrings.forEach(i -> {
            coderForEncoderTest.setString(i);
            assertNotEquals(i,coderForEncoderTest.encode());
        });
    }

    @Test
    public void decoderTest(){
        Coder encoderForDecoderTest = new Coder();
        Coder decoderForDecoderTest = new Coder();
        // Testen, ob der decoder den String tatsächlich dekodiert
        ArrayList<String> testStringsEncoded = new ArrayList<>();
        teststrings.forEach(i -> {
            encoderForDecoderTest.setString(i);
            testStringsEncoded.add(encoderForDecoderTest.encode());
        });
        assertEquals(teststrings.size(),testStringsEncoded.size());
        for (int i = 0; i<testStringsEncoded.size(); i++){
            decoderForDecoderTest.setString(testStringsEncoded.get(i));
            assertEquals(decoderForDecoderTest.decode(),teststrings.get(i));
        }
    }

    @Test
    public void codingCorrectnessTest(){
        final String PASSWORT = "testpassword123%";
        Coder Encoder = new Coder(PASSWORT);
        // Testen, ob der String nach Kodieren, entkodiert wieder den ursprünglichen String ergibt
        assertEquals(PASSWORT, new Coder(Encoder.encode()).decode());
    }

    @Test
    public void createFilesTest(){
        // Vor dem Erstellen des Ordners sollte dieser noch nicht existieren
        String DIRECTORYNAME = "testdirectory";
        assertFalse(Files.isDirectory(Paths.get(DIRECTORYNAME)));
        CreateFiles testCreator = new CreateFiles(DIRECTORYNAME);
        testCreator.createFiles();
        // Nach Aufruf der Methode sollte ein Ordner namens: "testdirectory" existieren
        assertTrue(Files.isDirectory(Paths.get(DIRECTORYNAME)));
        // Welcher folgende zwei Dateien beinhaltet: "journeys.tmp" und "templates.tmp", die für jeden User erzeugt werden.
        assertTrue(Files.exists(Path.of(DIRECTORYNAME + File.separator + "journeys.tmp")));
        assertTrue(Files.exists(Path.of(DIRECTORYNAME + File.separator + "templates.tmp")));
        // Beide sollten nach der Erstellung leer sein, da sie der User erst füllen muss
        assertEquals(new File(DIRECTORYNAME + File.separator + "journeys.tmp").length(), 0);
        assertEquals(new File(DIRECTORYNAME + File.separator + "templates.tmp").length(), 0);
        // Ordner löschen, sobald er nicht mehr gebraucht wird
        try { // https://www.baeldung.com/java-delete-directory
            Files.walk(Path.of("testdirectory"))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void journeyGetterTest() {
        // GETTER TEST
        Journey one = new Journey("03/04/2019", new Adress("Ludwigstraße", "35390", "40", "Gießen"),
                new Adress("Pliensaustraße", "73728", "14", "Esslingen"), 286.5, true, "Fahrt zum Testen");
        assertEquals(one.getDate(), "03/04/2019");
        assertEquals(one.getAdressorigin().getStreet(), "Ludwigstraße");
        assertEquals(one.getAdressorigin().getAreacode(), "35390");
        assertEquals(one.getAdressorigin().getHousenr(), "40");
        assertEquals(one.getAdressorigin().getCity(), "Gießen");
        assertEquals(one.getAdressdestination().getStreet(), "Pliensaustraße");
        assertEquals(one.getAdressdestination().getAreacode(), "73728");
        assertEquals(one.getAdressdestination().getHousenr(), "14");
        assertEquals(one.getAdressdestination().getCity(), "Esslingen");
        assertEquals(286.5, one.getDistance(), 0.0);
        assertFalse(one.isWork());
        assertTrue(one.isPrivat());
        assertEquals(one.getNote(), "Fahrt zum Testen");
    }

    @Test
    public void journeyKmAndKmstandTest() {
        // TOTAL KILOMETER TESTS
        // liste mit fahrten erstellen
        ArrayList<Journey> list = new ArrayList<>();
        list.add(new Journey("03/04/2019", new Adress("Ludwigstraße", "35390", "40", "Gießen"),
                new Adress("Pliensaustraße", "73728", "14", "Esslingen"), 286.5, true, "Erste Fahrt zum Testen"));
        list.add(new Journey("08/13/2018", new Adress("Kalsmunstraße", "35579", "20", "Wetzlar"),
                new Adress("Eckartstraße", "70101", "3/1", "Stuttgart"), 255, false, "Zweite Fahrt zum Testen"));
        list.add(new Journey("10/18/2017", new Adress("Bahnhofstraße", "73728", "45", "Esslingen"),
                new Adress("Eckartstraße", "70101", "28", "Stuttgart"), 13.8, false, "Dritte fahrt zum Testen"));
        // Double erstellen, welcher als fiktiver Startkilometerstand eines users benutzt wird
        final double STARTKM = 15000;
        // Anfangs ist noch kein Kmstand initialisiert, da der Anfangs kilometerstand unbekannt
        list.forEach(i -> {assert i.getKmstand() == Double.MAX_VALUE;});
        // Kilometerstände setzen
        setKilometerstände(list,STARTKM);
        // jetzt sollte keiner mehr nicht initialisiert(Double.MAX_VALUE) sein
        list.forEach(i -> {assert i.getKmstand() != Double.MAX_VALUE;});
        // nach erster fahrt -> Anfangskilometerstand addiert mit Fahrtdistanz voon Fahrt 1
        assert list.get(0).getKmstand() == STARTKM + list.get(0).getDistance();
        // nach zweiter fahrt -> Anfangskilometerstand addiert mit Fahrtdistanz vom Fahrt 1 und 2
        assert list.get(1).getKmstand() == STARTKM + list.get(0).getDistance() + list.get(1).getDistance();
        // nach dritter fahrt -> Anfangskilometerstand addiert mit Fahrtdistanz vom Fahrt 1 und 2 und 3
        assert list.get(2).getKmstand() == STARTKM + list.get(0).getDistance() + list.get(1).getDistance() + list.get(2).getDistance();
        // gesamt gefahrene kilometer alle drei strecken
        assert totalKm(list) == list.get(0).getDistance() + list.get(1).getDistance() + list.get(2).getDistance();
        // privat gefahrene kilometer alle drei strecken
        assert totalPrivatKm(list) == list.get(0).getDistance();
        // geschäftlich gefahrene kilometer alle drei strecken
        assert totalWorkKm(list) == list.get(1).getDistance() + list.get(2).getDistance();
        // geschäftlich gefahrene km + privat gefahrene km, sollten addiert gesamten gefahrenen km ergeben
        assert totalKm(list) == totalPrivatKm(list) + totalWorkKm(list);
    }

    // Das A und B vor den Tests bewirkt, dass sie in der richtigen Reihenfolge ausgeführt werden
    @Test
    public void BsaveStoreableTest()  {
        // Für den Test eine Liste vom Typ User(implementiert Storeable) erzeugen
        ArrayList<User> users = new ArrayList<>();
        users.add(new User("testuser1","testpassword1",10000.0));
        users.add(new User("testuser2","testpassword2",20000.0));
        users.add(new User("testuser3","testpassword3",30000.0));
        // LISTE SPEICHERN
        try {
            saveStoreable(users,"src" + File.separator +  "test" + File.separator + "java" + File.separator + "usersTest.tmp");
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Ordner sollte nun eine Datei namens "usersTest.tmp" enthalten
        assertTrue(Files.exists(Path.of("src" + File.separator + "test" + File.separator + "java" + File.separator + "usersTest.tmp")));
        // Die Datei sollte nicht leer sein
        assertNotEquals(0, new File("src" + File.separator + "test" + File.separator + "java" + File.separator + "usersTest.tmp").length());
    }

    // Eine private Methode zum Vergleich zweier User, genutzt in "AloadStoreableTest()"
    private boolean equals(User one, User two) {
        if (one == two) return true;
        if (one == null || two == null) return false;
        return Double.compare(one.getKilometres(), two.getKilometres()) == 0 &&
                Objects.equals(one.getUsername(), two.getUsername()) &&
                Objects.equals(one.getPassword(), two.getPassword());
    }

    @Test
    public void AloadStoreableTest()  {
        // Für den Test eine Liste vom Typ User(implementiert Storeable) erzeugen
        ArrayList<User> users = new ArrayList<>();
        users.add(new User("testuser1","testpassword1",10000.0));
        users.add(new User("testuser2","testpassword2",20000.0));
        users.add(new User("testuser3","testpassword3",30000.0));
        // ZUVOR GESPEICHERTE LISTE LADEN
        ArrayList<User> users2 = null;
        try {
            users2 = (ArrayList<User>) loadStoreable("src" + File.separator + "test" + File.separator + "java" + File.separator + "usersTest.tmp");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        // Liste sollte nicht leer sein
        assertFalse(users2.isEmpty());
        // Liste sollte von der Klasse ArrayList sein
        assertEquals("class java.util.ArrayList",users2.getClass().toString());
        // Jedes Element sollte vom Typ User sein
        users2.forEach(i -> assertEquals(i.getClass().toString(),"class pack.User"));
        // Checken, ob die beiden Listen die gleiche Länge haben
        assertEquals(users.size(),users2.size());
        // Jeder User in users sollte seinem Gegenstück in users2 gleichen
        for(int i = 0; i<users.size() ; i++){
            assertTrue(equals(users.get(i),users2.get(i)));
        }
        // Erstellte .tmp Datei wieder löschen
        new File("src" + File.separator + "test" + File.separator + "java" + File.separator + "usersTest.tmp").delete();
    }

    @Test
    public void compareTest(){
        // Testet, ob die compare Methode, der User Klasse, richtig funltioniert
        User testuser = new User("testname1","testpasswort1",100.0);
        // Username richtig, passwort nicht
        assertFalse(testuser.compare("testname1","solltefehlschlagen"));
        // Passwort richtig, username nicht
        assertFalse(testuser.compare("solltefehlschlagen","testpasswort1"));
        // Beides falsch
        assertFalse(testuser.compare("solltefehlschlagen","solltefehlschlagen"));
        // Richtige Angaben
        assertTrue(testuser.compare("testname1","testpasswort1"));
        // Zu Anschauungszwecken ein print
        Coder passwortDecoder = new Coder(testuser.getPassword());
        System.out.println("Vergleich, kodiert vs unkodiert zu Anschauungszwecken:\nUnkodiert: " + "\""+ passwortDecoder.decode() + "\"" + "\nKodiert: \""+testuser.getPassword()+ "\"");
    }

    @Test
    public void pathToFilesTest(){
        // Testet ob die Methoden den korrekten Pfad zurückgeben
        User testuser = new User("testname1","testpasswort1",100.0);
        String username = testuser.getUsername();
        final String CORRECTJOURNEYSPATH = username + File.separator + "journeys.tmp";
        final String CORRECTTEMPLATESPATH = username + File.separator + "templates.tmp";
        // Beide Pfade sollten den betriebsystemunabhängigen "Fileseparator enthalten"
        assertTrue(getJourneystmpPath(username).contains(File.separator));
        assertTrue(getTemplatestmpPath(username).contains(File.separator));
        // Sowie den Usernamen
        assertTrue(getJourneystmpPath(username).contains(testuser.getUsername()));
        assertTrue(getTemplatestmpPath(username).contains(testuser.getUsername()));
        // Und den Dateinamen
        assertTrue(getJourneystmpPath(username).contains("journeys.tmp"));
        assertTrue(getTemplatestmpPath(username).contains("templates.tmp"));

        assertEquals(CORRECTJOURNEYSPATH, getJourneystmpPath(username));
        assertEquals(CORRECTTEMPLATESPATH, getTemplatestmpPath(username));
    }
    @Test
    public void createsMdFiletest(){
        // Testet ob der EInsatz eines MarkdownConverters eine .md Datei erstellt.

        // Liste mit fahrten erstellen
        ArrayList<Journey> list = new ArrayList<>();
        list.add(new Journey("03/04/2019", new Adress("Ludwigstraße", "35390", "40", "Gießen"),
                new Adress("Pliensaustraße", "73728", "14", "Esslingen"), 286.5, true, "Erste Fahrt zum Testen"));
        list.add(new Journey("08/13/2018", new Adress("Kalsmunstraße", "35579", "20", "Wetzlar"),
                new Adress("Eckartstraße", "70101", "3/1", "Stuttgart"), 255, false, "Zweite Fahrt zum Testen"));
        list.add(new Journey("10/18/2017", new Adress("Bahnhofstraße", "73728", "45", "Esslingen"),
                new Adress("Eckartstraße", "70101", "28", "Stuttgart"), 13.8, false, "Dritte fahrt zum Testen"));

        MarkdownConverter mc = new MarkdownConverter(list, "testuser");
        mc.convertToMarkdown(System.getProperty("user.dir") + File.separator + "src" + File.separator +  "test" + File.separator + "BeispielMarkdownDatei");
        assertTrue(new File(System.getProperty("user.dir") + File.separator + "src" + File.separator +  "test" + File.separator + "BeispielMarkdownDatei.md").exists());
        assertTrue(new File(System.getProperty("user.dir") + File.separator + "src" + File.separator +  "test" + File.separator + "BeispielMarkdownDatei.md").getName().endsWith(".md"));
        // Beispieldatei wieder löschen
        new File(System.getProperty("user.dir") + File.separator + "src" + File.separator +  "test" + File.separator + "BeispielMarkdownDatei.md").delete();
    }
}