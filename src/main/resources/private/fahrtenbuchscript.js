var http = new XMLHttpRequest();

async function onload(){
    request('journeys');
    await new Promise(resolve => setTimeout(resolve, 100));
    request('templates');
}

function request(journeysortemplates){
    http.open('GET','request?request=' + journeysortemplates);
    http.send();
    http.onload = function() {
        if(journeysortemplates == "journeys") document.getElementById("fahrtentable").innerHTML = this.responseText;
        if(journeysortemplates == "templates") document.getElementById("templatetable").innerHTML = this.responseText;
    };
}

function logout() {
    http.open('GET','logout');
    http.send();
    http.onload = function() {if(http.status == 200) window.location.pathname = '/index.html';};
}

function useTemplate(index){
    http.open('GET','usetemplate?index='+index);
    http.send();
    http.onload = function() {
        var values = this.responseText.split(";");
        for(i = 0; i<10 ; i++) document.getElementById((i+1)+"").value = values[i];
        document.getElementById('privat').value = values[10] == 'true' ? 1 : 0;
    };
    document.getElementById('0').value = "";
    $('#addModal').modal('toggle');
}

function remove(index,journeysortemplates) {
    http.open('GET', 'remove' + journeysortemplates + '?index=' + index);
    http.send();
    http.onload = function () {if (http.status == 200) (journeysortemplates == 'templates') ? request('templates') : request('journeys');}
}

function markdownDownload() {
    http.open('GET', 'markdown?path=' + document.getElementById("downloadpath").value);
    http.send();
    document.getElementById("downloadpath").value = "";
    http.onload = function () {if (http.status == 200) onload();}
}

function addEntry(journey) {
    var data = new FormData();
    // 0 = datum, 1 = originStraße, 2= orHausnr, 3= orPLZ, 4=orStadt, 5=destinationStraße, 6=destHausnr, 7=destPLZ, 8=destStadt, 9=Distanz, ,10=Notiz
    for(i = 0; i<11 ; i++) data.append(''+i,document.getElementById(''+i).value);
    // 0 = geschäftlich, 1 = privat
    data.append('privat',document.getElementById('privat').value == 1);
    data.append('journey', journey);
    http.open('POST', 'add');
    // Kein Datum gesetzt -> kein Farhteintrag
    if (document.getElementById('0').value == "" && journey) return;
    http.send(data);
    http.onload = function() {
        if(http.status == 400) return;
        if(journey) request('journeys');
        if(!journey) request('templates');
    };
    $('#addModal').modal('hide');
}