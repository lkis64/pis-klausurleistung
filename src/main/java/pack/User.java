package pack;

class User implements Storeable, java.io.Serializable {
    private final String username;
    private final String password;
    private final double kilometres;

    User(String username, String password, double kilometres) {
        this.username = username;
        this.password = new Coder(password).encode();
        this.kilometres = kilometres;
    }

    String getUsername() {
        return this.username;
    }

    double getKilometres() {
        return this.kilometres;
    }

    String getPassword() {return password;}

    // Methode, die Benutzername und Passwort eines Users mit zwei String Argumenten vergleicht (für das Login))
    boolean compare(String username, String password) {
        Coder passwortCoder = new Coder(this.getPassword());
        return this.username.equals(username) && passwortCoder.decode().equals(password);
    }
}