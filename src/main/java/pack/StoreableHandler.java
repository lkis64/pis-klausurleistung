package pack;

import java.io.*;
import java.util.ArrayList;

class StoreableHandler {

    // Speichert eine ArrayList vom Typ Storeable in eine .tmp Datei
    static void saveStoreable(ArrayList<? extends Storeable> fahrten, String path) throws IOException {
        FileOutputStream fos = new FileOutputStream(path);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(fahrten);
        oos.close();
    }

    // Liest eine .tmp Datei aus und speichert den Inhalt wieder in einer ArrayList
    static ArrayList<? extends Storeable> loadStoreable(String path) throws IOException, ClassNotFoundException {
        File file = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (path.contains("journeys")) {
            return (file.length() == 0) ? new ArrayList<Journey>() : (ArrayList<Journey>) new ObjectInputStream(fis).readObject();
        }
        if (path.contains("templates")) {
            return (file.length() == 0) ? new ArrayList<JourneyTemplate>() : (ArrayList<JourneyTemplate>) new ObjectInputStream(fis).readObject();
        }
        if (path.contains("users")) {
            return (file.length() == 0) ? new ArrayList<User>() : (ArrayList<User>) new ObjectInputStream(fis).readObject();
        }
            return null;
    }
}