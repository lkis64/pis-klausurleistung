package pack;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

class CreateFiles {

    private final String userToCreateFor;

    CreateFiles(String userToCreateFor) {
        this.userToCreateFor = userToCreateFor;
    }

    private String getUserToCreateFor() {
        return userToCreateFor;
    }

    // Ordner mit den zwei benötigten Dateien für einen neuen Benutzer erstellen
    void createFiles() {
        String s = System.getProperty("user.dir") + File.separator + this.getUserToCreateFor();
        if (Files.exists(Path.of(s))) return;
        new File(s).mkdirs();
        try {
            new File(s + File.separator + "journeys.tmp").createNewFile();
            new File(s + File.separator + "templates.tmp").createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
