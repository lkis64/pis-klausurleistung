package pack;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

class Coder {
    private String string;

    Coder(String string) {
        this.string = string;
    }

    Coder() { }

    public void setString(String string) {
        this.string = string;
    }

    private String getString() { return string; }

    // Gegebenen Base64-kodierten String dekodieren
    String decode(){
        if (this.getString().equals("") || this.getString() == null) throw new IllegalArgumentException("CoderString can not be null or empty.");
        return new String(Base64.getDecoder().decode(this.getString()), StandardCharsets.UTF_8);
    }

    // Gegebenen String Base64 kodieren
    String encode(){
        if (this.getString().equals("") || this.getString() == null) throw new IllegalArgumentException("CoderString can not be null or empty.");
        return Base64.getEncoder().encodeToString(this.getString().getBytes(StandardCharsets.UTF_8));
    }
}

