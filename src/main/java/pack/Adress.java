package pack;

class Adress implements java.io.Serializable {
    private final String street,areacode,housenr,city;

    Adress(String street, String areacode, String housenr, String city) {
        this.street = street;
        this.areacode = areacode;
        this.housenr = housenr;
        this.city = city;
    }

    String getStreet() {return street;}

    String getAreacode() {return areacode;}

    String getHousenr() {return housenr;}

    String getCity() {return city;}
}
