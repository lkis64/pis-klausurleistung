package pack;

import java.io.IOException;
import java.util.ArrayList;
import static pack.StoreableHandler.*;

class UserManagement {
    private static ArrayList<User> users = new ArrayList<>();

    // Wenn der Username nicht leer oder bereits vergeben ist wird ein neuer Benutzer erstellt
    static boolean registerUser(String username, String password, int kilometres) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty() || kilometres < 0.0) return false;
        try {
            users = (ArrayList<User>) loadStoreable("users.tmp");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        if (users.stream().anyMatch(i -> i.getUsername().equals(username))) return false; //check for user duplicate
        new CreateFiles(username).createFiles();
        users.add(new User(username, password,kilometres));
        try {
            saveStoreable(users,"users.tmp");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    // Überprüft ob die angegebenen Daten mit denen eines bereits registrierten Nutzers übereinstimmen
    static boolean loginUser(String username, String password)  {
        try {
            users = (ArrayList<User>) loadStoreable("users.tmp");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return users.stream().anyMatch(i -> i.compare(username, password));
    }
}