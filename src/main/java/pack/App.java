package pack;

import io.javalin.Javalin;
import io.javalin.validation.JavalinValidation;

import java.io.File;
import java.util.*;
import static pack.Journey.journeyHtmlTableBuilder;
import static pack.JourneyTemplate.journeyTemplateHtmlTableBuilder;
import static pack.StoreableHandler.*;
import static pack.UserManagement.*;

public class App {

    // Liefert bei gegebenen Benutzername den Pfad zur journeys.tmp Datei
    static String getJourneystmpPath(String username){ return username + File.separator + "journeys.tmp"; }

    // Liefert bei gegebenen Benutzername den Pfad zur templates.tmp Datei
    static String getTemplatestmpPath(String username){ return username + File.separator + "templates.tmp"; }

    public static void main(String[] args) {
        Javalin app = Javalin.create().sessionHandler(Sessions::fileSessionHandler).enableStaticFiles("/public").enableStaticFiles("/private").start(7007);

//---------------------------------------------------------------------------INDEX:HTML--------------------------------------------------------------------------------------------

        //REGISTER
        app.post("/register", ctx -> {
            ArrayList<User> users = (ArrayList<User>) loadStoreable("users.tmp");
            String username = ctx.formParam("benutzername", String.class)
                    .check(i -> users.stream().noneMatch(user -> user.getUsername().equals(i))).get();

            JavalinValidation.validate(ctx.formParam("passwort")).notNullOrEmpty();
            int kilometer = ctx.formParam("kilometer",Integer.class).get();
            registerUser(username, ctx.formParam("passwort"),kilometer);

        });

        //LOGIN
        app.post("/login", ctx -> {
            JavalinValidation.validate(ctx.formParam("benutzername")).notNullOrEmpty();
            JavalinValidation.validate(ctx.formParam("passwort")).notNullOrEmpty();
            if (loginUser(ctx.formParam("benutzername"), ctx.formParam("passwort"))) {
                ctx.sessionAttribute("Username", ctx.formParam("benutzername"));
                ctx.status(200);
            } else {
                ctx.status(400);
            }
        });

//---------------------------------------------------------------------------FAHRTENBUCH:HTML--------------------------------------------------------------------------------------------

        //LOGOUT
        app.get("/logout", ctx -> ctx.req.getSession().invalidate());

        // TEMPLATES ODER FAHRTEN AKTUALISIEREN
        app.get("/request", ctx -> {
            String username = ctx.sessionAttribute("Username");
            String request = ctx.queryParam("request",String.class).check(i -> i.equals("journeys") || i.equals("templates")).get();
            ArrayList<String> innerHTMLS = new ArrayList<>();
            if (request.equals("journeys")) innerHTMLS = journeyHtmlTableBuilder((ArrayList<Journey>)loadStoreable(getJourneystmpPath(username)),username);
            if (request.equals("templates")) innerHTMLS = journeyTemplateHtmlTableBuilder((ArrayList<JourneyTemplate>)loadStoreable(getTemplatestmpPath(username)));
            StringBuilder tbSent = new StringBuilder();
            innerHTMLS.forEach(tbSent::append);
            ctx.result(tbSent.toString());
        });

        //TEMPLATE ODER FAHRT HINZUFÜGEN
        app.post("/add", ctx -> {
            String username = ctx.sessionAttribute("Username");
            String date,orStreet,orPLZ,orHouse,orCity,destStreet,destPLZ,destHouse,destCity;
            // 0 = datum, 1 = originStraße, 2= orHausnr, 3= orPLZ, 4=orStadt, 5=destinationStraße, 6=destHausnr, 7=destPLZ, 8=destStadt, 9=Distanz
            JavalinValidation.validate((date = ctx.formParam("0"))).notNullOrEmpty();
            JavalinValidation.validate((orStreet = ctx.formParam("1"))).notNullOrEmpty();
            JavalinValidation.validate((orHouse = ctx.formParam("2"))).notNullOrEmpty();
            JavalinValidation.validate((orPLZ = ctx.formParam("3"))).notNullOrEmpty();
            JavalinValidation.validate((orCity = ctx.formParam("4"))).notNullOrEmpty();
            JavalinValidation.validate((destStreet = ctx.formParam("5"))).notNullOrEmpty();
            JavalinValidation.validate((destHouse = ctx.formParam("6"))).notNullOrEmpty();
            JavalinValidation.validate((destPLZ = ctx.formParam("7"))).notNullOrEmpty();
            JavalinValidation.validate((destCity = ctx.formParam("8"))).notNullOrEmpty();
            double distance = ctx.formParam("9",Double.class).check(i -> i>0.0d).get();
            boolean privat = ctx.formParam("privat",Boolean.class).get();
            boolean journey = ctx.formParam("journey",Boolean.class).get();
            // Es handelt sich um eine Fahrt
            if (journey) {
                ArrayList<Journey> tmp = (ArrayList<Journey>) loadStoreable(getJourneystmpPath(username));
                tmp.add(new Journey(date, new Adress(orStreet, orPLZ, orHouse, orCity), new Adress(destStreet, destPLZ, destHouse, destCity), distance, privat, ctx.formParam("10")));
                saveStoreable(tmp, getJourneystmpPath(username));
            } else if (!journey){ // Es handelt sich um ein Template
                ArrayList<JourneyTemplate> tmp = (ArrayList<JourneyTemplate>) loadStoreable(getTemplatestmpPath(username));
                tmp.add(new JourneyTemplate(new Adress(orStreet, orPLZ, orHouse, orCity), new Adress(destStreet, destPLZ, destHouse, destCity), distance, privat, ctx.formParam("10")));
                saveStoreable(tmp, getTemplatestmpPath(username));
            }
        });

        //TEMPLATE NUTZEN
        app.get("/usetemplate", ctx -> {
            String username = ctx.sessionAttribute("Username");
            int index = ctx.queryParam("index",Integer.class).get();
            String path = getTemplatestmpPath(username);
            ArrayList<JourneyTemplate> journeyTemplates = (ArrayList<JourneyTemplate>) loadStoreable(path);
            // index ändern, weil liste in reverse dargestellt wird, siehe Methode "journeyHtmlTableBuilder" am Ende ; Geteilt durch zwei, weil nach jeder table row eine Leerzeile folgt
            int actualindex = journeyTemplates.size() - 1 - index/2;
            JourneyTemplate wantedTemplate = journeyTemplates.get(actualindex);
            StringJoiner joiner = new StringJoiner(";").add(wantedTemplate.getAdressorigin().getStreet()).add(wantedTemplate.getAdressorigin().getHousenr())
                    .add(wantedTemplate.getAdressorigin().getAreacode()).add(wantedTemplate.getAdressorigin().getCity()).add(wantedTemplate.getAdressdestination().getStreet())
                    .add(wantedTemplate.getAdressdestination().getHousenr()).add(wantedTemplate.getAdressdestination().getAreacode()).add(wantedTemplate.getAdressdestination().getCity())
                    .add(Double.toString(wantedTemplate.getDistance())).add(wantedTemplate.getNote()).add(Boolean.toString(wantedTemplate.isPrivat()));
            ctx.result(joiner.toString());
        });

        //EINZELNE FAHRT LÖSCHEN
        app.get("/removejourneys", ctx -> {
            String username = ctx.sessionAttribute("Username");
            int index = ctx.queryParam("index",Integer.class).get();
            String path = getJourneystmpPath(username);
            ArrayList<Journey> journeys = (ArrayList<Journey>) loadStoreable(path);
            // index ändern, weil liste in reverse dargestellt wird, siehe Methode "journeyTemplateHtmlTableBuilder" am Ende
            int actualindex = journeys.size() - 1 - index;
            journeys.remove(actualindex);
            saveStoreable(journeys,path);
        });

        //EINZELNES TEMPLATELÖSCHEN
        app.get("/removetemplates", ctx -> {
            String username = ctx.sessionAttribute("Username");
            int index = ctx.queryParam("index",Integer.class).get();
            String path = getTemplatestmpPath(username);
            ArrayList<JourneyTemplate> journeyTemplates = (ArrayList<JourneyTemplate>) loadStoreable(path);
            // index ändern, weil liste in reverse dargestellt wird, siehe Methode "journeyHtmlTableBuilder" am Ende ; Geteilt durch zwei, weil nach jeder table row eine Leerzeile folgt
            int actualindex = journeyTemplates.size() - 1 - index/2;
            journeyTemplates.remove(actualindex);
            saveStoreable(journeyTemplates,path);
        });

        // MARKDOWN DOWNLOAD
        app.get("/markdown", ctx -> {
            String username = ctx.sessionAttribute("Username");
            String path =  ctx.queryParam("path");
            // Pfad wenn nötig in korrektes Format bringen somit ist sowohl /home/test als auch /home/test/ gültig
            if (!path.endsWith(File.separator)) path += File.separator;
            path+= "Fahrten" + username;
            MarkdownConverter mc = new MarkdownConverter((ArrayList<Journey>)loadStoreable(getJourneystmpPath(username)),username);
            mc.convertToMarkdown(path);
        });

        app.exception(java.io.FileNotFoundException.class , (e, ctx) -> ctx.status(404));
    }
}
