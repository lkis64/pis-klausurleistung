package pack;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import static pack.Journey.*;

class MarkdownConverter {
    private final ArrayList<Journey> journeysToConvert;
    private static final ArrayList<String> table = new ArrayList<>( );
    private final String username;

    MarkdownConverter(ArrayList<Journey> journeysToConvert, String username) {
        this.journeysToConvert = journeysToConvert;
        this.username = username;
    }

    private ArrayList<Journey> getJourneysToConvert() {
        return journeysToConvert;
    }

    private String getUsername() {return username; }

    // Erstellt Überschrift, Info Zeile und Tabellenkopf
    private void parseHeader(){
        String totalKm = String.format("%.2f", totalKm(this.getJourneysToConvert()));
        String totalPrivatKm = String.format("%.2f",  totalPrivatKm(this.getJourneysToConvert()));
        String totalWorkKm = String.format("%.2f",  totalWorkKm(this.getJourneysToConvert()));
        table.add("## Fahrtenuebersicht\n\n" + "**" + this.getUsername() + "** <br> " + "**Gesamt-Kilometer**: " + totalKm + "km | **davon geschaeftlich**: " + totalWorkKm
                + "km | **davon privat**: " + totalPrivatKm + "km \n\n <br> "
                + "Datum | Abfahrtsadresse | Zieladresse | Kilometerstand <br> vor Fahrtantritt | Distanz | Kilometerstand <br> nach Fahrtantritt | Geschaeftsfahrt | Privatfahrt\n"
                + ":--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:"
        );}

    // Erstellt die Tabellenzeilen
    private void parseRows(){
        ArrayList<Journey> journeys = this.getJourneysToConvert();
        journeys.forEach(i -> {
            String privatZelle = i.isPrivat() ? "**X**" : "";
            String workZelle = i.isWork() ? "**X**" : "";
            String roundedAfterJourneyKm = String.format("%.2f", i.getKmstand());
            String roundedBeforeJourneyKm = String.format("%.2f", i.getKmstand()-i.getDistance());

            table.add(new StringJoiner(" | ").add(i.getDate()).add(i.getAdressorigin().getStreet() + " " + i.getAdressorigin().getHousenr()+ " <br> "
                    + i.getAdressorigin().getAreacode() + ", **"+i.getAdressdestination().getCity()+"**").add(i.getAdressdestination().getStreet() + " "
                    + i.getAdressdestination().getHousenr()+ " <br> " + i.getAdressdestination().getAreacode() + ", **"+i.getAdressdestination().getCity()+"**")
                    .add(""+(roundedBeforeJourneyKm+ "km")).add(""+i.getDistance()+ "km").add(""+roundedAfterJourneyKm + "km").add(workZelle).add(privatZelle).toString());
        });
    }

    // Erstellt die ArrayListe mit dem Markdown-Inhalt, welche anschließend man writeFile übergeben wird
    void convertToMarkdown(String path)  {
        table.clear();
        this.parseHeader();
        this.parseRows();
        try {
            writeFile( path + ".md",table);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Schriebt jedes Element der Liste(Parameter) als eine Zeile in eine Datei
    private void writeFile(String filename, List<String> lst) throws IOException {
        FileWriter writer = new FileWriter(filename);
        lst.forEach(s -> {
            try {
                writer.write(s + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.close();
    }
}
