package pack;

import java.util.*;

class JourneyTemplate implements java.io.Serializable, Storeable {

    private final Adress adressorigin,adressdestination;
    private final double distance;
    private final boolean privat;
    private final String note;

    JourneyTemplate(Adress adressorigin, Adress adressdestination, double distance, boolean privat, String note) {
        this.adressorigin = adressorigin;
        this.adressdestination = adressdestination;
        this.distance = distance;
        this.privat = privat;
        this.note = note;
    }

    Adress getAdressorigin() {return adressorigin;}

    Adress getAdressdestination() {return adressdestination;}

    double getDistance() {return distance;}

    boolean isPrivat() {return privat;}

    String getNote() {return note;}

    // Baut den tbody für die HTML Ansicht
    static ArrayList<String> journeyTemplateHtmlTableBuilder(ArrayList<JourneyTemplate> lst){
        ArrayList<String> innerHTMList = new ArrayList<>();
        lst.forEach(i -> {
            String note = i.getNote();
            String privatOrWork = " background-color: rgba(246, 246, 246, 0.86);";
            if (i.isPrivat()) privatOrWork = "background-color:rgba(40, 87, 110,0.5);";
            innerHTMList.add(
                    new StringBuilder("<tr>" + "<td onclick='useTemplate(($(this).parent().index()));' style=\"outline : 7px solid rgba(0, 0, 0, 0.3);" + privatOrWork + "cursor:copy; \">")
                    .append(i.getAdressorigin().getStreet()).append(" ").append(i.getAdressorigin().getHousenr()).append(" <br> ").append(i.getAdressorigin().getAreacode())
                    .append(",").append("<b>").append(i.getAdressorigin().getCity()).append("</b> <br>").append("<i class=\"fas fa-arrow-down\"></i>".repeat(10)).append(" <br> ")
                    .append(i.getAdressdestination().getStreet()).append(" ").append(i.getAdressdestination().getHousenr()).append("<br>")
                    .append(i.getAdressdestination().getAreacode()).append(",").append("<b>").append(i.getAdressdestination().getCity()).append("</b>")
                    .append(" <hr style = \"margin:2px;\"> <br> ").append("<b>Notiz: </b><i>".repeat(note.length() > 0 ? 1 : 0)).append(note).append("</i><b> | ".repeat(note.length() > 0 ? 1 : 0)).append("<b>Distanz: ").append(i.getDistance())
                    .append("km </b></td>").append("<td style = \"width : 20px; vertical-align : middle; border:none; background-color: transparent; \"><p style=\"color : red;cursor:pointer;\" onclick='remove(($(this).parent().parent().index()),\"templates\");' ><strong>X</strong></p></td></tr> <br>").toString()
            );
        });
        // Damit die neuen Vorlagen oben erscheinen
        Collections.reverse(innerHTMList);
        return innerHTMList;
    }
}
