package pack;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import static pack.StoreableHandler.*;
import static pack.App.getJourneystmpPath;

class Journey implements java.io.Serializable, Storeable {
    final static long serialVersionUID = 1L;

    private final String date,note;
    private final Adress adressorigin,adressdestination;
    private double kmstand = Double.MAX_VALUE ;
    private final double distance;
    private final boolean work,privat;

    Journey(String date, Adress adressorigin, Adress adressdestination, double distance, boolean privat, String note) {
        if(distance <= 0) throw new IllegalArgumentException("Distance must be positive!");
        this.date = date;
        this.adressorigin = adressorigin;
        this.adressdestination = adressdestination;
        this.distance = distance;
        this.privat = privat;
        this.work = !privat;
        this.note = note;
    }

    //GETTER
    String getDate() {return date;}

    Adress getAdressorigin() {return adressorigin;}

    Adress getAdressdestination() {return adressdestination;}

    double getKmstand() {return kmstand; }

    double getDistance() {return distance;}

    boolean isWork() {return work;}

    boolean isPrivat() {return privat;}

    String getNote() {return note;}

    // SETTER
    private void setKmstand(double kmstand) {this.kmstand = kmstand;}

    // Setzt mithilfe des Startkilometerstandes die Kilometerstände für eine Liste von Fahrten
    static void setKilometerstände(ArrayList<Journey> lst, double userSignupKm){
        lst.forEach(i -> {
            // wenn nur eine fahrt da -> anfangskilometerstand + neue distanz
            if (lst.indexOf(i) == 0) i.setKmstand(userSignupKm + i.getDistance());
            // ansonsten vorheriger + neue distanz
            if (lst.indexOf(i) >= 1) i.setKmstand(lst.get(lst.indexOf(i) - 1).getKmstand() + i.getDistance());
        });
    }

    // Liefert die gesamt privat zurückgelegten Kilometer einer Liste von Fahrten
    static double totalPrivatKm(ArrayList<Journey> lst){
        return lst.stream().filter(Journey::isPrivat).map(Journey::getDistance).mapToDouble(i->i).sum();
    }
    // Liefert die gesamt geschäftlich zurückgelegten Kilometer einer Liste von Fahrten
    static double totalWorkKm(ArrayList<Journey> lst){
        return lst.stream().filter(Journey::isWork).map(Journey::getDistance).mapToDouble(i->i).sum();
    }

    // Liefert die gesamt zurückgelegten Kilometer einer Liste von Fahrten
    static double totalKm(ArrayList<Journey> lst){
        return lst.stream().map(Journey::getDistance).mapToDouble(i->i).sum();
    }

    // Baut den tbody für die HTML Ansicht
    static ArrayList<String> journeyHtmlTableBuilder(ArrayList<Journey> lst, String user) throws IOException, ClassNotFoundException {
        ArrayList<String> innerHTMList = new ArrayList<>();
        // get currentuser, his index and his signup KM
        ArrayList<User> users = (ArrayList<User>)loadStoreable("users.tmp");
        int currentUserIndex = Integer.MAX_VALUE;
        for (int i = 0 ; i<users.size() ; i++){
            if(users.get(i).getUsername().equals(user)) currentUserIndex = i;
        }
        User currentuser = users.get(currentUserIndex);
        double userSignupKm = currentuser.getKilometres();
        String username = currentuser.getUsername();
        setKilometerstände(lst,userSignupKm);
        saveStoreable(lst,getJourneystmpPath(username));
        double previousKmstand = userSignupKm;
        for (Journey journey : lst){
            String note = journey.getNote();
            String privatOrWork = " style = \"background-color: rgba(246, 246, 246, 0.86);\"";
            if(journey.isPrivat()) privatOrWork = " style = \"background-color:rgba(40, 87, 110,0.5);\"";
            String roundedBeforeJourneyKm = String.format("%.2f", previousKmstand );
            String roundedAfterJourneyKm = String.format("%.2f", journey.getKmstand());
            innerHTMList.add(
                    new StringBuilder("<tr" + privatOrWork + ">").append("<td style=\"font-size: 18pt;\">").append(journey.getDate()).append("</td>").append(" <td style=\"font-size: 18pt;\">")
                            .append(journey.adressorigin.getStreet()).append(" ").append(journey.adressorigin.getHousenr()).append("<br>").append(journey.adressorigin.getAreacode())
                            .append(", ").append("<strong>").append(journey.adressorigin.getCity()).append("</strong>").append("</td>").append(" <td style=\"font-size: 18pt;\">")
                            .append(journey.adressdestination.getStreet()).append(" ").append(journey.adressdestination.getHousenr()).append("<br>").append(journey.adressdestination.getAreacode())
                            .append(", ").append("<strong>").append(journey.adressdestination.getCity()).append("</strong>").append("</td>").append("<td style=\"font-size: 18pt;\">")
                            .append(roundedBeforeJourneyKm).append("km </td>").append("<td style=\"font-size: 18pt;\">").append(journey.getDistance()).append("km </td>").append("<td style=\"font-size: 18pt;\">")
                            .append(roundedAfterJourneyKm).append("km </td>").append("<td style=\"font-size: 18pt; overflow:auto;\">").append("<i>").append(note).append("</i>").append("</td>")
                            .append("<td><button id ='trash1' class ='btn btn-danger' onclick='remove(($(this).parent().parent().index()),\"journeys\");'><strong>X</strong></button></td></tr>")
                            .toString()
            );
            previousKmstand = journey.getKmstand();
        }
        // Damit die neuen Fahrten oben erscheinen
        Collections.reverse(innerHTMList);
        return innerHTMList;
    }
}
