var http = new XMLHttpRequest();

function zoom(){document.body.style.zoom="0.8";}

async function onload(){
    zoom();
    request('journeys');
    await new Promise(resolve => setTimeout(resolve, 100));
    request('templates');
}

function request(journeysortemplates){
    http.open('GET','request?request=' + journeysortemplates);
    http.send();
    http.onload = function() {
        if(journeysortemplates == "journeys") document.getElementById("fahrtentable").innerHTML = this.responseText;
        if(journeysortemplates == "templates") document.getElementById("templatetable").innerHTML = this.responseText;
    };
}

function logout() {
    http.open('GET','logout');
    http.send();
    http.onload = function() {
        if(http.status == 200) window.location.pathname = '/index.html';
    };
}

function useTemplate(index){
    http.open('GET','usetemplate?index='+index);
    http.send();
    http.onload = function() {
        var values = this.responseText.split(";");
        // 0 = datum, 1 = originStraße, 2= orHausnr, 3= orPLZ, 4=orStadt, 5=destinationStraße, 6=destHausnr, 7=destPLZ, 8=destStadt, 9=Distanz, 10=Fahrtauswahl(dropdown menü),11=Notiz
        for(i = 0; i<11 ; i++) document.getElementById((i+1)+"").value = values[i];
        document.getElementById('10').value = values[9] == 'true' ? 1 : 0;
    };
}

function remove(index,journeysortemplates) {
    http.open('GET', 'remove' + journeysortemplates + '?index=' + index);
    http.send();
    http.onload = function () {
        if (http.status == 200) (journeysortemplates == 'templates') ? request('templates') : request('journeys');
    }
}

function markdownDownload() {
    http.open('GET', 'markdown?path=' + document.getElementById("downloadpath").value);
    http.send();
    document.getElementById("downloadpath").value = "";
}

function addEntry(fahrtauswahl) {
    document.getElementById('fahrtentable').style.visibility = "visible";
    // 0 = Geschäftsfahrt, 1= Privatfahrt, 3 = Geschäftsfahrt Vorlage, 4= Privatfahrt Vorlage
    var data = new FormData();
    // 0 = datum, 1 = originStraße, 2= orHausnr, 3= orPLZ, 4=orStadt, 5=destinationStraße, 6=destHausnr, 7=destPLZ, 8=destStadt, 9=Distanz, 10=Fahrtauswahl(dropdown menü),11=Notiz
    for(i = 0; i<12 ; i++) data.append(''+i,document.getElementById(''+i).value);
    http.open('POST', 'add');
    // Kein Datum gesetzt -> kein Farhteintrag
    if (document.getElementById('0').value == "" && fahrtauswahl != 2 && fahrtauswahl != 3) return;
    http.send(data);
    http.onload = function() {
        if(http.status == 400) return;
        if(fahrtauswahl == 0 || fahrtauswahl == 1 )request('journeys');
        if(fahrtauswahl == 2 || fahrtauswahl == 3 )request('templates');
    };
}
