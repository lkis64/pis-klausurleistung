var http = new XMLHttpRequest();
// Login bei Enter-Taste
document.addEventListener("keydown", function () { if (event.keyCode == 13) login();});


function registrierAbschicken() {
    var data = new FormData();
    data.append('benutzername', document.getElementById('loginUser').value);
    data.append('passwort', document.getElementById('loginPasswort').value);
    data.append('kilometer', document.getElementById('registerkilometres').value);
    http.open('POST', 'register', true);
    http.send(data);
}

function login() {
    var data = new FormData();
    data.append('benutzername', document.getElementById('loginUser').value);
    data.append('passwort', document.getElementById('loginPasswort').value);
    http.open('POST', 'login',true);
    http.send(data);
    http.onload = function () {
        if (http.status == 200) window.location.pathname = '/fahrtenbuch.html';
    }
}

