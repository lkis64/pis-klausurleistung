# Projekt: Odos - Die Fahrtenbuch App (Fr/1, Hb)

Name & Praktikumstermin: Leon Ismaiel, 5234059 (Fr/1, Hb)


## Inhaltsverzeichnis


- [Projekt: Odos - Die Fahrtenbuch App (Fr/1, Hb)](#Projekt-Odos---Die-Fahrtenbuch-App-Fr1-Hb)
  - [Inhaltsverzeichnis](#Inhaltsverzeichnis)
  - [Kurzbeschreibung inkl. Screenshot](#Kurzbeschreibung-inkl-Screenshot)
  - [Beschreibung des Projektaufbaus](#Beschreibung-des-Projektaufbaus)
    - [Abgabedateien (LOC)](#Abgabedateien-LOC)
    - [Testdateien (TST)](#Testdateien-TST)
    - [Aufbau der Anwendung](#Aufbau-der-Anwendung)
  - [Dokumentation des implementierten WebAPIs](#Dokumentation-des-implementierten-WebAPIs)
    - [1. HTTP-Request-Response Paare des Login- und Registriersystems (`index.html`)](#1-HTTP-Request-Response-Paare-des-Login--und-Registriersystems-indexhtml)
      - [1.1 Registrierung](#11-Registrierung)
        - [Request](#Request)
        - [Response](#Response)
      - [1.2 Login](#12-Login)
        - [Request](#Request-1)
        - [Response](#Response-1)
    - [2. HTTP-Request-Response Paare Fahrtenbuchs (`fahrtenbuch.html`)](#2-HTTP-Request-Response-Paare-Fahrtenbuchs-fahrtenbuchhtml)
      - [2.1 Aktualsierungs-Anfrage](#21-Aktualsierungs-Anfrage)
        - [Request](#Request-2)
        - [Response](#Response-2)
      - [2.2 Hinzufügen von Fahrten oder Fahrt-Vorlagen](#22-Hinzuf%C3%BCgen-von-Fahrten-oder-Fahrt-Vorlagen)
        - [Request](#Request-3)
        - [Response](#Response-3)
      - [2.3 Vorlage nutzen](#23-Vorlage-nutzen)
        - [Request](#Request-4)
        - [Response](#Response-4)
      - [2.4 Fahrten oder Fahrt-Vorlage löschen](#24-Fahrten-oder-Fahrt-Vorlage-l%C3%B6schen)
        - [Request](#Request-5)
        - [Response](#Response-5)
      - [2.5 Markdown Converter](#25-Markdown-Converter)
        - [Request](#Request-6)
        - [Response](#Response-6)
      - [2.6 Logout](#26-Logout)
        - [Request](#Request-7)
        - [Response](#Response-7)
  - [Dokumentation des Interfaces](#Dokumentation-des-Interfaces)
    - [Adress.java](#Adressjava)
    - [Coder.java](#Coderjava)
    - [CreateFiles.java](#CreateFilesjava)
    - [Journey.java](#Journeyjava)
    - [JourneyTemplate.java](#JourneyTemplatejava)
    - [Sessions.java](#Sessionsjava)
    - [Storeable.java](#Storeablejava)
    - [StoreableHandler.java](#StoreableHandlerjava)
    - [User.java](#Userjava)
    - [UserManagement.java](#UserManagementjava)
    - [MarkdownConverter.java](#MarkdownConverterjava)
  - [Technischer Anspruch (TA) und Umsetzung der Features](#Technischer-Anspruch-TA-und-Umsetzung-der-Features)
    - [`.tmp`-Dateisystem](#tmp-Dateisystem)
    - [JavalinValidation](#JavalinValidation)
    - [Session-Handling](#Session-Handling)
  - [Quellennachweis](#Quellennachweis)
    - [1. Allgemeine Quellen](#1-Allgemeine-Quellen)
    - [2. Explizite Quellen](#2-Explizite-Quellen)
  - [Skizzen-Entwürfe der grafischen Oberfläche - Erste Design-Ansätze](#Skizzen-Entw%C3%BCrfe-der-grafischen-Oberfl%C3%A4che---Erste-Design-Ans%C3%A4tze)




## Kurzbeschreibung inkl. Screenshot

> Namentlich angelehnt an das alt- und auch neugriechische Wort ὁδός (dt. "Reise", beziehungsweise  "Straße" oder "Weg") soll die Fahrtenbuch App **Odos** eine Plattform bieten, welche es ermöglicht einfach, 
digital und überall ein Fahrtenbuch zu führen. <br> Die Applikation ist auf mehrere Benutzer ausgelegt, daher verfügt sie über ein Registrier- und Einloggsystem. Beim Registriervorgang werden die Zugangsdaten und
der aktuelle Kilometerstand gesetzt. Intern wird das Passwort durch Base64-Kodierung verschlüsselt und mit dem zugehörigen Benutzername gespeichert.  <br> Anschließend kann der Benutzer sein digitales Fahrtenbuch führen, der Fortschritt wird gespeichert und es kann beim nächsten Login dort weitergearbeitet werden, wo der Benutzer aufgehört hat. 
<br> Neben den essenziellen Eckdaten einer Fahrt können diese zusätzlich auch mit Notizen versehen werden. Wichtige, beziehungsweise oft auftretende, Fahrten können als Vorlage gespeichert werden, um das Arbeiten zu erleichtern. 
<br> Möchte der Benutzer seine Fahrtentabelle, entweder zum Einreichen oder für sich selber,
speichern oder drucken, kann er dies mithilfe des integrierten Markdown Dokument-Erstellers machen.


![Screenshot](InActionCollage.jpg)

<br> 

**Hinweise**: 
* Ihr Zugang zum Testen der Anwendung: **Benutzername**: *testuser*, **Passwort**: *testuser*. <br> Die Registrier-Funktion kann selbstverständlich trotzdem genutzt werden, um einen neuen Benutzer zu erstellen. 
* Der JavaScript-Code teilt sich, wie mit Professor Herzberg vereinbart, auf **zwei** Dateien auf: `fahrtenscript.js` im `private`Ordner und `indexscript.js` im `public` Ordner, somit wird sauber zwischen der Login-Ansicht und der Fahrtenbuch-Ansicht getrennt. 
* Bei den Tests wurden mehrere Tests in Methoden gekapselt, um die Tests logisch zu unterteilen.


---


## Beschreibung des Projektaufbaus

### Abgabedateien (LOC)

Verlinkter Dateiname | Dateiart | LOC
---------------------|----------|-----
**[Adress.java](src/main/java/pack/Adress.java)** | Java | 14
**[App.java](src/main/java/pack/App.java)** | Java | 110
**[Coder.java](src/main/java/pack/Coder.java)** | Java | 22
**[CreateFiles.java](src/main/java/pack/CreateFiles.java)** | Java | 25
**[Journey.java](src/main/java/pack/Journey.java)** | Java | 83
**[JourneyTemplate.java](src/main/java/pack/JourneyTemplate.java)** | Java | 39
**[MarkdownConverter.java](src/main/java/pack/MarkdownConverter.java)** | Java | 63
**[Sessions.java](src/main/java/pack/Sessions.java)** | Java | 21
**[StoreableHandler.java](src/main/java/pack/StoreableHandler.java)** | Java | 30
**[Storeable.java](src/main/java/pack/Storeable.java)** | Java | 2
**[User.java](src/main/java/pack/User.java)** | Java | 22
**[UserManagement.java](src/main/java/pack/UserManagement.java)** | Java | 32
**[indexscript.js](src/main/resources/public/indexscript.js)** | JavaScript | 18
**[fahrtenbuchscript.js](src/main/resources/private/fahrtenbuchscript.js)** | JavaScript | 56
**[index.html](src/main/resources/public/index.html)** | HTML | 34
**[fahrtenbuch.html](src/main/resources/private/fahrtenbuch.html)** | HTML | 148
**[indexstyles.css](src/main/resources/public/indexstyles.css)** | CSS | 44
**[fahrtenbuchstyles.css](src/main/resources/private/fahrtenbuchstyles.css)** | CSS | 30
**[hintergrundIndex.jpg](src/main/resources/public/hintergrundIndex.jpg)** | JPG | -
**[hintergrundFahrten.jpg](src/main/resources/private/hintergrundFahrten.jpg)** | JPG | -
 <br> | <br> | *= 793* 


### Testdateien (TST)

Verlinkter Dateiname | Testart | Anzahl der Tests
---------------------|---------|-----------------
**[FahrtenbuchTest.java](src/test/java/FahrtenbuchTest.java)** | JUnit4 | 53

Die Tests werden wie folgt ausgeführt: Im Terminal wird auf der Ebene der `build.gradle` die Eingabe `gradle test` getätigt.



### Aufbau der Anwendung 

![](FileTree.png)

Der Projektordner `src` teilt sich, wie auch oben anhand des FileTrees veranschaulicht,
in zwei weitere Ordner auf: `main` und `test`. <br> Im `test` Ordner befindet sich eine Datei namens `FahrtenbuchTest.java`, diese enthält alle JUnit4 Tests zum Projekt. Nach Ablauf der Tests wird von Gradle ein Bericht über den Ablauf erstellt, welcher unter dem Pfad: `build/reports/tests/test/index.html` als HTML-Dokument zu finden ist. <br>
Der `main` Ordner enthält den eigentlichen Programmcode der Anwendung im Ordner `java` befinden sich alle `.java` Dateien, der Kern der Anwendung. Die Inhalte aller `.java` Dateien wird in der folgenden Tabelle beschrieben:

<br>


| Datei | Inhalt |
| ----- | ----- |
**[Adress.java](src/main/java/pack/Adress.java)** | Einfache Klasse zur Erstellung von Adressen-Objekten in Java. 
**[App.java](src/main/java/pack/App.java)** | Bildet mit der main-Methode den Einstiegspunkt zum Serverstart. <br>Nimmt alle einkommenden HTTP-Requests entgegen um diese weiter zu verarbeiten.
**[Coder.java](src/main/java/pack/Coder.java)** | Nachdem eine Instanz mit einem `String` als Argument erzeugt wurde, kann dieser `String`, mithilfe der Methoden der Instanz, kodiert oder dekodiert werden.
**[CreateFiles.java](src/main/java/pack/CreateFiles.java)** | Wird mit dem Namen des Benutzers, für welchen ein Dateien-Ordner erstellt werden soll, instanziiert. Anschließend kann, über eine Methode der Instanz, ein Ordner mit den nötigen Dateien erstellt werden.
**[Journey.java](src/main/java/pack/Journey.java)** | *Implementiert [Storeable](src/main/java/pack/Storeable.java).*  Java-Klasse zur Erstellung von Fahrten-Objekten in Java, sie besitzt alle nötigen Parameter einer Fahrt, wie beispielsweise die Adressen und das Datum. Außerdem befinden sich Methoden zur Berechnung von gesamt-gefahrenen Kilometern einer Liste von Fahrten sowie eine Methode zum Parsen von HTML, um die Fahrten darzustellen.
**[JourneyTemplate.java](src/main/java/pack/JourneyTemplate.java)** | *Implementiert [Storeable](src/main/java/pack/Storeable.java).*  Java-Klasse zur Erstellung von Fahrt-Vorlage-Objekten, besitzt alle Parameter wie eine Fahrt, bis auf das Datum. Somit können wiederverwendbare, datumsunabhängige Vorlagen erstellt werden. Wie bei `Journeys.java` befindet sich auch in dieser Klasse eine statische Methode zum Parsen von HTML.
**[MarkdownConverter.java](src/main/java/pack/MarkdownConverter.java)** | Enthält Methoden, welche für eine gegebene Liste von Fahrten eines Benutzers ein individuelles Markdown-Dokument erstellt. Dieses kann unter einem beliebigen Pfad gespeichert werden.
**[Sessions.java](src/main/java/pack/Sessions.java)** | Enthält Methoden zum Erstellen eines SessionHandlers sowie Speicherort für diese Daten. Die statische Methode `fileSessionHandler` wird in der `App.java` beim Starten des Servers aufgerufen.
**[StoreableHandler.java](src/main/java/pack/StoreableHandler.java)** | Enthält zwei statische Methoden, zum Speichern und zum Laden von Java `ArrayLists` in `.tmp` Dateien. Die Methoden nehmen durch die Anwendung von Wildcards nur `ArrayLists` von Typen, welche `Storeable` implementieren, entgegen.
**[Storeable.java](src/main/java/pack/Storeable.java)** | Ein leeres Interface, welches von Klassen implementiert werden kann. Dadurch werden `ArrayLists` von diesen Klassen bei den Methoden in `StoreableHandler.java` ein gültiges Argument sein.
**[User.java](src/main/java/pack/User.java)** | *Implementiert [Storeable](src/main/java/pack/Storeable.java).* Eine Java-Klasse zum Erstellen von User-Objekten, welche Benutzername, kodiertes Passwort und Anfangskilometerstand als Parameter haben. Enthält ebenfalls eine Methode zum Abgleich von angegebenen Strings und den eigentlichen Daten eines Benutzers.
**[UserManagement.java](src/main/java/pack/UserManagement.java)** | Enthält eine statische Methode zum Registrieren eines neuen Benutzers und eine statische Methode zum Einloggen eines bereits registrierten Benutzers.

<br> 

Der Ordner `resources` beinhaltet den Code für das GUI (`.html` und `.css`) und die beiden JavaScript Dateien zum Senden von HTTP-Requests und Verarbeiten der HTTP-Responses des Servers. <br>
Im `private` Ordner befinden sich diese drei Bausteine für die eigentliche Fahrtenbuch-Ansicht und im `public` Ordner für das Login-und Registriersystem.



## Dokumentation des implementierten WebAPIs

Die WebAPI lässt sich grundlegend in zwe Teile gliedern:
1. JavaScript für **HTTP-Requests**
2. Java für **HTTP-Responses** <br>

Die HTTP-Requests findet man demnach in den Dateien `fahrtenbuchscript.js` und `indexscript.js` und die HTTP-Resposnes in der Datei `App.java`.
Um mittels JavaScript die Möglichkeit zu haben, HTTP-Requests zu senden, muss zuerst ein Objekt vom Typ `XMLHttpRequest` erstellt werden. 
Dieses dient anschließend mit seinen Funktionen `open()`, welche eine Anfrage initialisiert und `send()`, welche die Anfrage versendet, als Grundlage für diverse Request-Funktionen der Anwendung.
<br> So findet sich am Anfang der beiden `.js` Dateien folgende Variable: 
<br>
```javascript
var http = new XMLHttpRequest();
```
<br>

Außerdem gilt es, bei der Erstellung einer WebAPI, bei den Anfragen jeweils zwischen den beiden Anfrage-Methoden `GET` und `POST` zu wählen.
In dieser Anwendung wurde bei vertraulichem Datentransfer (wie beispielsweise das Senden des Passwortes) sowie bei Anfragen mit größeren Mengen
an Daten die `POST`-Methode gewählt und andernfalls die `GET`-Methode.

<br>

Im Folgenden werden die HTTP-Requests und -Requests immer als Paar, für eine bestimmte Funktionalität der Anwendung, erläutert. 

### 1. HTTP-Request-Response Paare des Login- und Registriersystems (`index.html`)

#### 1.1 Registrierung

##### Request
Für das Senden der Anfrage zur Registrierung eines neuen Benutzers ist die JavaScript-Funktion `registrierungAbschicken()` zuständig: <br>
```javascript
function registrierungAbschicken() {
    var data = new FormData();
    data.append('benutzername', document.getElementById('loginUser').value);
    data.append('passwort', document.getElementById('loginPasswort').value);
    data.append('kilometer', document.getElementById('registerkilometres').value);
    http.open('POST', 'register', true);
    http.send(data);
}
```
Mithilfe des Interfaces `FormData` werden bei der Registrierung eines neuen Benutzers die drei wichtigen Registrierungsparameter 
Benutzername, Passwort und aktueller Kilometerstand versendet. Nachdem ein Objekt von `FormData` erstellt wurde, werden diesem die Schlüssel-Wert-Paare
aus ID und dem vom Benutzer angegebenen Wert über die Funktion `append()` hinzugefügt. Anschließend wird die Anfrage mittels `POST`
(Vertrauliche Information) über den Pfad `/register` gesendet. 

##### Response
In der `App.java` wird die Anfrage mittels der `post()`-Methode des Javalin Servers und dem zugehörigen Pfad `/register` abgefangen. Mittels JavalinValidation und einer, durch
die statische Methode `loadStoreable()` der Klasse `StoreableHandler` geladenen, `ArrayList` wird geprüft, ob der gewünschte Benutzername bereits vergeben ist.
> Die Funktionsweis von [JavalinValidation](#javalinvalidation) wird unter dem Punkt *Features* erläutert.
```java
app.post("/register", ctx -> {
    ArrayList<User> users = (ArrayList<User>) loadStoreable("users.tmp"); //Das Laden der Liste
    String username = ctx.formParam("benutzername", String.class)
                    .check(i ->users.stream().map(user -> user.getUsername().equals(i)).noneMatch(result ->  result == true)).get();
```
Das Passwort und der angegebene Kilometerstand werden ebenfalls überprüft und anschließend, wenn alle Parameter in Ordnung sind, die
Methode `registerUser()` der Klasse `UserManagement` aufgerufen.
```java
    JavalinValidation.validate(ctx.formParam("passwort")).notNullOrEmpty();
    int kilometer = ctx.formParam("kilometer",Integer.class).get();
    registerUser(username, ctx.formParam("passwort"),kilometer);
});

```

#### 1.2 Login

##### Request

Bei dieser Funktion werden, erneut mittels `FormData`, zwei Schlüssel-Wert-Paare mit Benutzername und Passwort gesendet. Sobald die 
HTTP-Request beantwortet wurde (`http.onload`), wird überprüft, ob Code **200 - OK** gesandt wurde. <br> Ist dies der Fall war der Login erfolgreich und
der Benutzer wird zu seinem Fahrtenbuch weitergeleitet.

```javascript
function login() {
    var data = new FormData();
    data.append('benutzername', document.getElementById('loginUser').value);
    data.append('passwort', document.getElementById('loginPasswort').value);
    http.open('POST', 'login',true);
    http.send(data);
    http.onload = function () {
        if (http.status == 200) window.location.pathname = '/fahrtenbuch.html';
    }
}
```
##### Response
In der `App.java` wird die Anfrage abgefangen und die beiden gesendeten Parameter validiert. <br> Mit diesen wird die `loginUser()`-Methode
der Klasse `UserManagement` aufgerufen. Da diese Methode eine boolean zurückgibt, wird sie anschließend genutzt, um den richtigen Statuscode
zu übermitteln.
```java
app.post("/login", ctx -> {
    JavalinValidation.validate(ctx.formParam("benutzername")).notNullOrEmpty();
    JavalinValidation.validate(ctx.formParam("passwort")).notNullOrEmpty();
    if (loginUser(ctx.formParam("benutzername"), ctx.formParam("passwort"))) {
        ctx.sessionAttribute("Username", ctx.formParam("benutzername"));
        ctx.status(200);
    } else {
        ctx.status(400);
    }
}); 
```
Außerdem wird der Nutzername als SessionAttribut gesetzt.
> Die Funktionsweise von [Session-Handling](#session-handling) wird unter dem Punkt *Features* erläutert.

### 2. HTTP-Request-Response Paare Fahrtenbuchs (`fahrtenbuch.html`)

#### 2.1 Aktualsierungs-Anfrage

##### Request
Das Anfragen einer aktuellen Version einer Tabelle (Fahrten oder Fahrt-Vorlagen) geschieht über die `request()`-Funktion. 
Je nachdem welche Tabelle angefordert wird, wird die funktion entweder mit dem Parameter *"templates"* oder *"journeys*" aufgerufen.
```javascript
function request(journeysortemplates){
    http.open('GET','request?request=' + journeysortemplates);
    http.send();
    http.onload = function() {
        if(journeysortemplates == "journeys") document.getElementById("fahrtentable").innerHTML = this.responseText;
        if(journeysortemplates == "templates") document.getElementById("templatetable").innerHTML = this.responseText;
    };
}
```
Sobald eine Antwort kam, wird der jeweilige Tabelleninhalt, welcher über `document.getElementById(...)` referenziert wird, mit dem neuen,
vom Server stammenden, Inhalt ersetzt. Somit wird nach jeder Änderung oder beim Login die aktuelle Version der Tabelle dargestellt.


##### Response
```java
app.get("/request", ctx -> {
    String username = ctx.sessionAttribute("Username");
    String request = ctx.queryParam("request",String.class).check(i -> i.equals("journeys") || i.equals("templates")).get();
    ArrayList<String> innerHTMLS = new ArrayList<>();
    if (request.equals("journeys")) innerHTMLS = journeyHtmlTableBuilder((ArrayList<Journey>)loadStoreable(getJourneystmpPath(username)),username);
    if (request.equals("templates")) innerHTMLS = journeyTemplateHtmlTableBuilder((ArrayList<JourneyTemplate>)loadStoreable(getTemplatestmpPath(username)));
    StringBuilder tbSent = new StringBuilder();
    for (String s : innerHTMLS) tbSent.append(s);
    ctx.result(tbSent.toString());
});
```
Anfangs wird der aktuell eingeloggte User mithilfe der Session abgefragt und der Parameter `"request"`, mit der Bedingung, dass 
es sich um eine der beiden Optionen (`journeys` oder `templates`) handeln muss, validiert. <br>
Anschließend wird durch die jeweils gewünschten HTML-Parser-Methode der Klasse `Journey` oder `JourneyTemplate` eine `ArrayList` erstellt, dessen
Elemente als ein langer `String` übertragen werden. Dieser lange `String`, die aktualisierte Tabelle als HTML, ist dann der `String`, welcher den obigen Tabelleninhalt ersetzt.


#### 2.2 Hinzufügen von Fahrten oder Fahrt-Vorlagen

##### Request
Das Hinzufügen einer Fahrt oder eine Fahrt-Vorlage beinhaltet das Übertragen von allen Angaben, welche der Benutzer gemacht hat. <br>
Dazu gehören die folgenden: Datum (*ausschließlich für Fahrten*), Startadresse(Straße, Hausnr., Stadt, PLZ), Zieladresse(Straße, Hausnr., Stadt, PLZ), Distanz und Notiz.
Die Input-Felder oben genannten 10 Angaben wurden intern mit Zahlen als ID's versehen, um das Hinzufügen zur `FormData` effizient zu gestalten.
```javascript
...
for(i = 0; i<11 ; i++) data.append(''+i,document.getElementById(''+i).value);
...
```

Ein weiterer Parameter, welcher der `FormData` hinzugefügt wird ist `privat`. Dieser ist ein `boolean` und gibt an, ob es sich um eine private Fahrt, bzw. Vorlage handelt.
```javascript
...
data.append('privat',document.getElementById('privat').value == 1);
...
```

![](Select.jpg)
> Das HTML-Select Element, bei welchem der Benutzer den Parameter `privat` setzt.

Die angegebenen Daten können nun über zwei Buttons an den Server gesendet werden. Einer davon fügt eine Vorlage hinzu, der andere eine Fahrt.
Diese Information wird der Funktion als Argument übergeben.  <br>
Es handelt sich um einen `boolean`, ist dieser `true` handelt es sich 
um eine Fahrt, ansonsten um einen Eintrag für die Fahrt-Vorlagen.
Wenn es eine Fahrt werden soll (`journey == true`), aber kein Datum angegeben ist, wird der Vorgang abgebrochen:
```javascript
if (document.getElementById('0').value == "" && journey) return;
```

<br>

Mittels `http.onload` wird nach der HTTP-Response mit der `request()` Funktion die relevante Tabelle aktualisiert:
```javascript
http.onload = function() {
        if(http.status == 400) return;
        if(journey)request('journeys');
        if(!journey)request('templates');
};
```

##### Response
In der zugehörigen `app.post()` werden anfangs der aktuelle Username abgefragt und anschließend alle empfangenen Daten validiert.
```java
app.post("/add", ctx -> {
    String username = ctx.sessionAttribute("Username");
    String date,orStreet,orPLZ,orHouse,orCity,destStreet,destPLZ,destHouse,destCity;
    JavalinValidation.validate((date = ctx.formParam("0"))).notNullOrEmpty();
                            ........
    double distance = ctx.formParam("9",Double.class).check(i -> i>0.0d).get();
    boolean privat = ctx.formParam("privat",Boolean.class).get();
    boolean journey = ctx.formParam("journey",Boolean.class).get();
```
Anschließend wird überprüft, ob es sich um eine Fahrt oder Fahrt-Vorlage handelt, je nachdem wird die relevante Liste über 
`loadStoreable()` geladen, über die `add()`-Methode der `ArrayList` erweitert und wieder gespeichert (`saveStoreable`).
Dieses Konstrukt sieht dann wie folgt aus: <br>

```java
    ArrayList<Journey | JourneyTemplate> tmp = (ArrayList<Journey | JourneyTemplate>) loadStoreable(getJourneystmpPath(username) | getTemplatesPath(username));
    tmp.add(new Journey(...) | new JourneyTemplate(...));
    saveStoreable(tmp, getJourneystmpPath(username) | getTemplatesPath(username));
```
In der `App.java` wurde der oben stehende Code mit einer if-else if-Abfrage geschrieben. <br>
Die relevanten .tmp-Dateien wurden erfolgreich bearbeitet und können nun, wie man im JavaScript Code sieht, angefordert werden.

#### 2.3 Vorlage nutzen
> Vorlagen werden genutzt, indem man auf sie anklickt. Es wird sich das übliche Modal zur Hinzufügung öffnen, alle Felder sind bereits mit den Daten der Vorlage ausgefüllt.

##### Request
Es wird zunächst eine `GET`-Anfrage mit dem Index der Fahrt-Vorlage, welche genutzt werden soll, gesandt. <br>
Sobald die HTTP-Response ankam, können die empfangenen Werte erneut bequem in einer for-Schleife angewandt werden.
```javascript
function useTemplate(index){
    http.open('GET','usetemplate?index='+index);
    http.send();
    http.onload = function() {
        var values = this.responseText.split(";");
        // 0 = datum, 1 = originStraße, 2= orHausnr, 3= orPLZ, 4=orStadt, 5=destinationStraße, 6=destHausnr, 7=destPLZ, 8=destStadt, 9=Distanz, 10=Fahrtauswahl(dropdown menü),11=Notiz
        for(i = 0; i<11 ; i++) document.getElementById((i+1)+"").value = values[i];
        document.getElementById('10').value = values[9] == 'true' ? 1 : 0;
    };
}
```
So muss lediglich das Datum ausgefüllt werden, um aus der Fahrt-Vorlage eine Fahrt zu erstellen.

##### Response
```java
 app.get("/usetemplate", ctx -> {
     String username = ctx.sessionAttribute("Username");
     int index = ctx.queryParam("index",Integer.class).get();
     String path = getTemplatestmpPath(username);
     ArrayList<JourneyTemplate> journeyTemplates = (ArrayList<JourneyTemplate>) loadStoreable(path);
       
```
Wie oben zu sehen wird wie üblich der Benutzername über die Session abgefragt, der Parameter (hier der Index) validiert und die relevante Liste geladen.
Wichtig ist jedoch, da die HTML-Parser-Methoden so arbeiten, dass sie den Inhalt extra umgekehrt widergeben, dass der Index angepasst wird. <br>
Außerdem ist zwischen jeder Tabellen-Zeile eine Leerzeile, daher wird der ursprüngliche Index zuerst durch zwei geteilt.
```java
    int actualindex = journeyTemplates.size() - 1 - index/2;
```
Dies wird gemacht, damit neue Fahrten oben erscheinen und nicht unten. Zu guter Letzt wird mittels eines `StringJoiners` und dem Delimeter ";" 
ein `String` aus der gewünschten Fahrt-Vorlage erzeugt und mit `ctx.result()` versendet.
```java
    StringJoiner joiner = new StringJoiner(";").add(wantedTemplate.getAdressorigin().getStreet()).add(wantedTemplate.getAdressorigin().getHousenr())
            .add(wantedTemplate.getAdressorigin().getAreacode()).add(wantedTemplate.getAdressorigin().getCity()).add(wantedTemplate.getAdressdestination().getStreet())
            .add(wantedTemplate.getAdressdestination().getHousenr()).add(wantedTemplate.getAdressdestination().getAreacode()).add(wantedTemplate.getAdressdestination().getCity())
            .add(Double.toString(wantedTemplate.getDistance())).add(Boolean.toString(wantedTemplate.isPrivat())).add(wantedTemplate.getNote());
    ctx.result(joiner.toString());
});
```



#### 2.4 Fahrten oder Fahrt-Vorlage löschen

Das Löschen einer Fahrt und das Löschen einer Fahrt-Vorlage ist sehr ähnlich.  <br> 
Im JavaScript Code gibt es eine Funktion für beide Löschvorgänge und im Java Code sind die beiden Methoden sehr ähnlich aufgebaut.
Daher werden sie hier unter einem Punkt behandelt.

##### Request
Da es für beide Löschvorgänge nur eine Funktion gibt, hat diese **zwei** Parameter. Den Index, an welchem das zu entfernende Element steht 
und um welche Art von Element es sich handelt (Fahrt oder Fahrt-Vorlage).

```javascript
function remove(index,journeysortemplates) {
    http.open('GET', 'remove' + journeysortemplates + '?index=' + index);
    http.send();
    http.onload = function () {
        (journeysortemplates == 'templates') ? request('templates') : request('journeys');
    }
}
```
Diese beiden Parameter werden in der URL über die `GET`-Methode gesendet. Sobald die HTTP-Response kam, wird die relevante Tabelle, mithilfe von `request()`, aktualisiert.

Die `app.get()`-Methoden sind, wie oben erwähnt, sehr ähnlich. Im Folgenden wird das Beispiel der Fahrt-Vorlagenlöschung erläutert:
##### Response
```java
 app.get("/removetemplates", ctx -> {
     String username = ctx.sessionAttribute("Username");
     int index = ctx.queryParam("index",Integer.class).get();
     String path = getTemplatestmpPath(username);
     ArrayList<JourneyTemplate> journeyTemplates = (ArrayList<JourneyTemplate>) loadStoreable(path);
```
Wie üblich wird der aktuelle Benutzername abgefragt, der Index, welcher als Parameter empfangen wurde, validiert und die relevante `ArrayList` geladen.
```java
    int actualindex = journeyTemplates.size() - 1 - index/2;
    journeyTemplates.remove(actualindex);
    saveStoreable(journeyTemplates,path);
});
```
Wie schon in der Anfrage eine Fahrt-Vorlage zu nutzen, muss der Index, aufgrund der Differenz zwischen der Darstellung der Fahrten auf der Oberfläche und der internen Speicherung, angepasst werden.
Über die `remove()`-Methode der `ArrayList` wird das gewünschte Element entfernt und die überarbeitete Liste anschließend wieder gespeichert.

#### 2.5 Markdown Converter

##### Request
Die JavaScript-Funktion, welche die Anfrage stellt, ein Markdown-Dokument der Fahrten zu speichern, ist sehr simpel. Sie sendet ein Anfrage mit 
dem vom Benutzer angegebenen Pfad als Parameter von `path`. Anschließend wird das HTML Feld wieder entleert, um den Platzhalter wieder sichtbar zu machen.
```javascript
function markdownDownload() {
    http.open('GET', 'markdown?path=' + document.getElementById("downloadpath").value);
    http.send();
    document.getElementById("downloadpath").value = "";
}
```

##### Response
In der HTTP-Response wird nach dem Abfragen des Benutzernamens der aktuellen Session und dem Erstellen des Pfades, mithilfe des erhaltenen Parameters und einem Dateinamen,
ein Objekt der Klasse `MarkdownConverter` erstellt. Dieses erwartet im Konstruktor sowohl die Liste der Fahrten des Benutzers als auch den Benutzernamen.
Anschließend wird die Methode `convertToMarkdown` des Objektes aufgerufen, um eine Datei mit der Fahrten-Liste im Markdown Format zu erstellen.
```java
app.get("/markdown", ctx -> {
    String username = ctx.sessionAttribute("Username");
    String path =  ctx.queryParam("path") + "Fahrten" + username +".md";
    MarkdownConverter mc = new MarkdownConverter((ArrayList<Journey>)loadStoreable(getJourneystmpPath(username)),username);
    mc.convertToMarkdown(path);
});
```

#### 2.6 Logout

##### Request
Auch diese Funktion ist sehr simpel, nachdem über `http.open()` der Logout des aktuellen Benutzers eingeleitet wird, wird der Benutzer, sobald eine HTTP-Response
erhalten wurde, auf die Startseite weitergeleitet.  
```javascript
function logout() {
    http.open('GET','logout');
    http.send();
    http.onload = function() {
        if(http.status == 200) window.location.pathname = '/index.html';
    };
}
```

##### Response
Das einzige, was auf Server-Seite geschieht, ist, dass die aktuelle Session des Benutzers invalidiert wird:
```java
app.get("/logout", ctx -> ctx.req.getSession().invalidate());
```
Nach Erfolg wird im JavaScript-Code die Weiterleitung auf die Startseite eingeleitet.


## Dokumentation des Interfaces

Im folgenden befindet sich eine Erläuterung der `.java`-Dateien, dem funktionalen Kern der Anwendung. Exkludiert ist die Datei `App.java` , da sie unter dem Punkt [WebAPI](#webapi) behandelt 
wurde. <br>

### Adress.java
Die Klasse `Adress` ist sehr simpel. Die Klasse besitzt vier Instanzvariablen vom Typ `String`: Straße, Hausnummer, PLZ und Stadt. Es wurde unter 
Berücksichtigungen von ausländische Postleitzahlen, welche nicht nur Ziffern enthalten können sowie Hausnummern mit Schräg- oder Bindestrich für jede dieser Instanzvariablen
der Typ `String` gewählt. <br>
Neben dem Konstruktor existieren lediglich Getter-Methoden zu den Instanzvariablen.


### Coder.java
Die `Coder`-Klasse besitzt eine Instanzvariable vom Typ `String` und zwei Konstruktoren. Ein Standardkonstruktor und einer, in welchem dieser `String` instanziiert wird. 
Durch die Setter-Methode für die Instanzvariable kann die Instanzvariable geändert werden oder aber auch erst nach der Instanziierung gesetzt werden. <br>
Auf ein `Coder`-Objekt können zwei Methoden angewandt werden, `decode()` und `encode()`. Wie der Name es verspricht, wird die jeweilige Instanzvariable 
Base64-dekodiert oder Base64-kodiert zurückgegeben. <br>

```java
                                        ... 
// Gegebenen Base64-kodierten String dekodieren
String decode(){
    if (this.getString().equals("") || this.getString() == null) throw new IllegalArgumentException("CoderString can not be null or empty.");
    return new String(Base64.getDecoder().decode(this.getString()), StandardCharsets.UTF_8);
}

// Gegebenen String Base64 kodieren
String encode(){
    if (this.getString().equals("") || this.getString() == null) throw new IllegalArgumentException("CoderString can not be null or empty.");
    return Base64.getEncoder().encodeToString(this.getString().getBytes(StandardCharsets.UTF_8));
}
                                        ...
```
Die De- und Kodierung wird mithilfe von Java's Base64-Klasse durchgeführt. 

### CreateFiles.java
Die `CreateFiles.java` enhthält die gleichnamige Klasse `CreateFiles`. Mit einem Objekt dieser Klasse können der notwendige Ordner sowie die darin benötigten Dateien für 
neue Benutzer erstellt werden. Dem Konstruktor übergibt man den Benutzernamen, für welchen ein Ordner erstellt werden soll. Für diese Instanzvariable existiert eine Getter-Methode. <br>

```java
                                        ... 
private String userToCreateFor;
CreateFiles(String userToCreateFor) {this.userToCreateFor = userToCreateFor;}
private String getUserToCreateFor() {return userToCreateFor;}

                                        ...
```

Wendet man die Methode `createFiles()` auf dieses Objekt an, wird ein Ordner mit dem Wert der Instanzvariable `userToCreateFor` erstellt, welcher eine Datei namens "journeys.tmp" und eine namens "templates.tmp" enthält.

```java
void createFiles() {
    String s = System.getProperty("user.dir") + File.separator + this.getUserToCreateFor();
    if (Files.exists(Path.of(s))) return;
    new File(s).mkdirs();
    try {
        new File(s + File.separator + "journeys.tmp").createNewFile();
        new File(s + File.separator + "templates.tmp").createNewFile();
    } catch (IOException e) {
        e.printStackTrace();
    }
}
```


### Journey.java
Diese Klasse implementiert `Storeable`. <br>
Der Hauptbestandteil eines Fahrtenbuchs sind die Fahrten-Objekte. Für diese gibt es die Klasse `Journey`. Als Instanzvariablen hat ein solches Objekt alle nötigen Informationen, welche
eine Fahrt haben muss: <br>
* Abfahrtsadresse (`Adress`-Objekt : Straße, Hausnummer, PLZ, Stadt)
* Zieladresse (`Adress`-Objekt : Straße, Hausnummer, PLZ, Stadt)
* Kilometerstand (nach Ankunft)
* Datum 
* Notiz
* Distanz
* Zwei boolsche Werte zur Speicherung des Reisezweckes (`work` und `privat`)

Zu jeder Instanzvariable gibt es eine Getter-Methode. Für die Instanzvariable Kilometerstand gibt es ebenfalls eine Setter-Methode, da der Kilometerstand erst sobald eine Liste aus Fahrten mit Startkilometerwert besteht, berechnet werden kann. Deshalb sind alle Instanzvariablen bis auf diese `final`. <br>
Außerdem enthält die Methode vier statische Methoden, welche für `ArrayLists` vom Typ `Journey` ausgelegt sind. So zum Beispiel die Methode `setKilometerstände()`, welche mit gegebener `ArrayList`
vom Typ `Journey` und einen Startkilometerstand (`double`) aufgerufen wird. Diese Methode setzt für jede Fahrt den korrekten Kilometerstand. <br>
```java
static void setKilometerstände(ArrayList<Journey> lst, double userSignupKm){
    lst.forEach(i -> {
        // wenn nur eine fahrt da -> anfangskilometerstand + neue distanz
        if (lst.indexOf(i) == 0) i.setKmstand(userSignupKm + i.getDistance());
        // ansonsten vorheriger + neue distanz
        if (lst.indexOf(i) >= 1) i.setKmstand(lst.get(lst.indexOf(i) - 1).getKmstand() + i.getDistance());
    });
}
```

Außerdem gibt es drei Methoden, welche die Gesamtkilometer, privat gefahrenen Kilometer oder geschäftlich gefahrenen Kilometer einer `ArrayList` von Fahrteinträgen berechnen. <br>
Alle drei `double`-Rückgabewerte wurden mit Einsatz eines Streams berechnet. Im Folgengen gezeigt am Beispiel der geschäftlich gefahrenen Kilometer: <br>

```java
static double totalWorkKm(ArrayList<Journey> lst){
    return lst.stream().filter(Journey::isWork).map(Journey::getDistance).mapToDouble(i->i).sum();
}
```

Aus allen existierenden Farteinträgen werden die herausgefiltert, bei welchen es sich um Privatfahrten handelt (Methode `isWork()`). Anschließend wird durch die Methodenreferenz in der `map()`-Methode
und der `sum()` Methode die Summe all dieser Werte als `double` zurückgegeben. <br>

Bei der letzten Methode der Klasse handelt es sich um eine statische Methode zur Erstellung von HTML-Tabellenzeilen von `Journey`-Objekten namens `journeyHtmlTableBuilder()`. In dieser Methode wird
mithilfe der `forEach()` Methode einer `ArrayList` von `Journey`-Objekten (Parameter der Methode) für jedes Element eine Tabllenzeile `<tr>...</tr>` erzeugt. Diese wird als `String`
in eine `ArrayList` gespeichert. Eine solche `ArrayList` wird am Ende der Methode zurückgegeben und kann über die `App.java` versendet werden. Es könnten, da es sich um eine `ArrayList` handelt, auch einzelne Zeilen der Tabelle
gesendet werden. 

Bevor sie zurückgegeben wird, wird die `ArrayList` durch 
```java
Collections.reverse(ARRAYLIST);
```
umgekehrt, sodass neue Fahrteinträge immer an erster Stelle, also im GUI ganz oben, erscheinen.


### JourneyTemplate.java
Diese Klasse implementiert `Storeable`. <br>
Objekte der Klasse `JourneyTemplate` sollen Vorlagen für Fahrteinträge sein, daher besitzt `JourneyTemplate` die gleichen Instanzvariablen der Klasse `Journey`, bis auf `String date`. 
Für diese Instanzvariablen gibt es jeweils Getter-Methoden: <br>

```java
                                        ... 
private final Adress adressorigin,adressdestination;
                                        ... // Weitere Instanzvariablen
JourneyTemplate(Adress adressorigin, Adress adressdestination, double distance, boolean privat, String note) {
    this.adressorigin = adressorigin;
    this.adressdestination = adressdestination;
    this.distance = distance;
    this.privat = privat;
    this.note = note;
}

Adress getAdressorigin() {return adressorigin;}
                                        ... // Weitere Getter-Methoden
```

`JourneyTemplate` enthält genau wie `Journey` ebenfalls eine Methode zur Erstellung von HTML-Tabellenzeilen.
Wie bei der Klasse `Journey` wird die `ArrayList` von HTML-Tabellenzeilen auch bei dieser Methode in umgekehrter Reihenfolge zurückgegeben.

### Sessions.java
Die `Sessions.java` beinhaltet den nötigen Code, um Sessions zu verwalten. [Siehe "Session-Handling"](#session-handling).

### Storeable.java
Das Interface Storeable ist ist leer und dient lediglich dazu, in der Klasse `StoreableHandler` Wildcards zu nutzen. Eine Erläuterung folgt im nächsten Punkt, [StoreableHandler.java](#storeablehandlerjava).

### StoreableHandler.java
In der Klasse `StoreableHandler` befinden sich zwei statische Methoden, `saveStoreable()` und `loadStoreable()`. Beide Methoden dienen dazu `ArrayListen` 
in `.tmp`-Dateien zu Speichern bzw. zu Laden. Daher haben beide Methoden als Parameter eine `ArrayList` vom Typ `? extends Storeable`. Dadurch sind die Methoden nur für 
`ArrayListen`-Argumente vom Typ `User`, `Journey`, oder `JourneyTemplate` nutzbar, da nur diese drei Klassen `Storeable` implementieren. <br>
Das Speichern sowie das Laden mittels Out- und Inputstreams wird unter dem Punkt [`.tmp`-Dateisystem](#tmp-dateisystem) erläutert. <br>
Bei der `loadStoreable()` Methode muss das vom `OutputInputStream` gelesene Objekt noch gecastet werden. Je nachdem welche Datei im Pfad angegeben wurde, wird das Objekt auf den korrekten `ArrayList`-Typen gecastet. Ist die Datei leer, wird eine leere `ArrayList` zurückgegeben: <br>
```java
                                        ...

if (path.contains("journeys")) {
    return (file.length() == 0) ? new ArrayList<Journey>() : (ArrayList<Journey>) new ObjectInputStream(fis).readObject();
}
if (path.contains("templates")) {
    return (file.length() == 0) ? new ArrayList<JourneyTemplate>() : (ArrayList<JourneyTemplate>) new ObjectInputStream(fis).readObject();
}
if (path.contains("users")) {
    return (file.length() == 0) ? new ArrayList<User>() : (ArrayList<User>) new ObjectInputStream(fis).readObject();
}
                                        ...
```

### User.java
Diese Klasse implementiert `Storeable`. <br>
Ein Objekt der Klasse `User` hat drei **finale** Instanzvaribablen, denn diese sollen nach der Registrierung nicht mehr verändert werden. <br> 
Im Konstruktor werden die zwei `Strings` `username` und `password` und der `double kilometres`, welcher für den aktuellen Kilometerstand bei der Registrierung steht, gesetzt.
Das bei der Instanziierung übergebene Passwort wird, mithilfe eines `Coder`-Objekts, verschlüsselt gesetzt: <br>
```java
                                        ...
                                        
User(String username, String password, double kilometres) {
    this.username = username;
    this.password = new Coder(password).encode();  
    this.kilometres = kilometres;
}
                                        ...
```
Für diese drei Instanzvariablen existieren Getter-Methoden. Der letzte Bestandteil dieser Klasse ist die Methode `compare()`, welche zwei `String` 
Argumente erwartet. Sie vergleicht mittels eines `Coder`-Objektes das entschlüsselte Passwort des Users und den Benutzernamen mit dem jeweils übergebenen 
`String`, diese Methode wird beim Login genutzt. Bei Übereinstimmung beider `Strings` wird `true` zurückgegeben, ansonsten `false`. <br>
```java
                                        ...
                                        
boolean compare(String username, String password) {
        Coder passwortCoder = new Coder(this.getPassword());
        return this.username.equals(username) && passwortCoder.decode().equals(password);
}
                                        ...
```

        
### UserManagement.java
Die Klasse `UserManagement` enthält eine Instanzvariable, eine `ArrayList` vom Typ `User`, und zwei statische Methoden. <br>
In der ersten Methode, `registerUser()` wird den bereits vorhandenen Benutzern, welche in der Datei `users.tmp` gespeichert sind, ein weiterer 
Benutzer hinzugefügt. <br> Als Argumente bekommt die Methode die drei Registrierungsangaben Benutzername, Passwort und Startkilometerstand gegeben. 
Sind die Angaben ungültig (z.B. leerer Benutzername, leeres Passwort etc.) wird `false` zurückgegeben. Ansonsten wird die aktuelle Liste von Benutzern mit der Methode 
`loadStoreable` aus der Klasse `StoreableHandler` geladen. <br>

```java
 static boolean registerUser(String username, String password, int kilometres) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty() || kilometres < 0.0) return false;
        try {
            users = (ArrayList<User>) loadStoreable("users.tmp");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
                                        ... 
```
 Sofern der Benutzername nicht bereits vergeben ist (if-Abfrage im nächsten Code-Segment), wird für diesen ein neuer Ordner erstellt. Außerdem wird er der `ArrayList` der registrierten Benutzer hinzugefügt. 
 Diese wird nach der Aktualisierung gespeichert.
```java
    if (users.stream().anyMatch(i -> i.getUsername().equals(username))) return false; //check for user duplicate
    new CreateFiles(username).createFiles();
    users.add(new User(username, password,kilometres));
    try {
        saveStoreable(users,"users.tmp");
    } catch (IOException e) {
        e.printStackTrace();
    }
    return true;
}
```
 
In der zweiten Methode, `loginUser()` wird geprüft, ob es eine Benutzer mit der Kombination aus Benutzername und Passwort, wie sie als Argumente übergeben werden, existiert.
Dazu wird erneut die `ArrayList` der aktuell registrierten Benutzer geladen und diese Bedingung mithilfe eines Streams und der `compare()` Methode überprüft.

```java
static boolean loginUser(String username, String password)  {
    try {
        users = (ArrayList<User>) loadStoreable("users.tmp");
    } catch (IOException | ClassNotFoundException e) {
        e.printStackTrace();
    }
    return users.stream().anyMatch(i -> i.compare(username, password));
}
```
War die Überprüfung erfolgreich, wird `true` zurückgegeben.
 
 
 

### MarkdownConverter.java

Über ein MarkDownConverter-Objekt ist es möglich eine Liste von Fahrten, wie sie in der Anwendung vorhanden ist, als Markdown-Dokument zu speichern. 
<br> Dem Konstruktor wird eine `ArrayList` der Fahrten (Typ `Journey`) und der Benutzername als `String` übergeben. Auf dieses Objekt kann die 
Methode `parseHeader()` angewandt werden, welche eine Übersicht der Kilometer und den Tabellenkopf in Markdown-Syntax erstellt. <br>
```java
                                        ... // Initialisierung der genutzten String-Objekte (totalKm,totalPrivatKm)
                                        
table.add("## Fahrtenuebersicht\n\n" + "**" + this.getUsername()) + "** <br> " + "**Gesamt-Kilometer**: " + totalKm + "km | **davon geschaeftlich**: " + totalWorkKm
        + "km | **davon privat**: " + totalPrivatKm + "km \n\n <br> "
        + "Datum | Abfahrtsadresse | Zieladresse | Kilometerstand <br> vor Fahrtantritt | Distanz | Kilometerstand <br> nach Fahrtantritt | Geschaeftsfahrt | Privatfahrt\n"
        + ":--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:"

                                        ...
```

Der Tabellenkopf wird anschließend der Instanzvariable `table`, einer `ArrayList`, hinzugefügt.
<br> Mit der Methode `parseRows()` werden der Tabelle alle Zeilen hinzugefügt. <br> 
Dies funktioniert über die `forEach()` Methode der `ArrayList`. Es wird für jedes 
Element eine Markdown-Tabellenzeile als `String` erstellt und `table` hinzugefügt. Die Markdown-Syntax wird mit einem `StringJoiner` und dem Delimiter `|`
eingehalten.

```java

ArrayList<Journey> journeys = this.getJourneysToConvert();
journeys.forEach(i -> {
                                                ... // Initialisierung der genutzten String-Objekte (privatZelle, workZelle etc.)
                                                
    table.add(new StringJoiner(" | ").add(i.getDate()).add(i.getAdressorigin().getStreet() + " " + i.getAdressorigin().getHousenr()+ " <br> "
            + i.getAdressorigin().getAreacode() + ", **"+i.getAdressdestination().getCity()+"**").add(i.getAdressdestination().getStreet() + " "
            + i.getAdressdestination().getHousenr()+ " <br> " + i.getAdressdestination().getAreacode() + ", **"+i.getAdressdestination().getCity()+"**")
            .add(""+(roundedBeforeJourneyKm+ "km")).add(""+i.getDistance()+ "km").add(""+roundedAfterJourneyKm + "km").add(workZelle).add(privatZelle).toString());
});
```


Wendet man die `convertToMarkdown()`-Methode auf das MarkdownConverter-Objekt an, wird die Datei an dem gewünschten Pfad (Argument der Methode) erzeugt. <br>
In dieser Methode werden die Methoden `parseHeader()` und `parseRows()`aufgerufen und anschließend die `ArrayList` Zeile für Zeile in eine Datei geschrieben.


```java
void convertToMarkdown(String path)  {
    table.clear();
    this.parseHeader();
    this.parseRows();
    try {
        writeFile( path,table);
    } catch (IOException e) {
        e.printStackTrace();
    }
}
```

Die private Methode `writeFile` ist für das Schreiben der Datei mittels der Java `FileWriter` Klasse zuständig.

> Beispiel für ein durch den MarkdownConverter generiertes Dokument:
> <br>
> <br>
> ![](MCPreview.jpg)

## Technischer Anspruch (TA) und Umsetzung der Features

Ich habe folgende Features verwendet. Die verlinkte Datei zeigt beispielhaft den Einsatz dieses Features in den angegebenen Zeilen im Quellcode.

1. Bootstrap, [fahrtenbuch.html](src/main/resources/private/fahrtenbuch.html) (65-161)
2. Streams, [UserManagement.java](src/main/java/pack/UserManagement.java) (18, 36)
3. Speicherung/Abruf von Daten im lokalen Dateisystem, [StoreableHandler.java](src/main/java/pack/StoreableHandler.java) (komplette Datei)
4. Validation, [App.java](src/main/java/pack/App.java) (72-83)
5. Session-Handling [Sessions.java](src/main/java/pack/Sessions.java) (komplette Datei)

### `.tmp`-Dateisystem
Die komplette Funktionalität des Dateisystems ist in der Klasse `StoreableHandler` realisiert. 
Die Motivation ein solches Dateisytsem zu erstellen war, Java Objekte so zu speichern, dass sie ohne weiteres 
wieder als Java Objekt abgerufen werden können. Über die Java-Klassen `FileOutputStream`, `FileInputStream`, `ObjectOutputStream` und 
`ObjectInputStream,` ist dies möglich.
 #### Speichern
 Die vorgehensweise zum Speichern eines Objektes sieht wie folgt aus: <br>
 1. Ein `FileOutputStream`-Objekt wird mit dem gewünschten Pfad als Argument erstellt.
 2. Ein `ObjectOutputStream`-Objekt wird mit dem erstellten `FileOutputStream`-Objekt als Argument erstellt.
 3. Zum Schreiben des Streams in eine Datei wird die Methode `writeObject()` auf das `ObjectOutputStream`-Objekt 
 angewandt. Als Argument erwartet diese das zu speichernde Objekt.(In diesem Projekt eine `ArrayList`)
 4. Der `ObjectOutputStream` wird geschlossen.
```java
FileOutputStream fos = new FileOutputStream(PATH); // 1.
ObjectOutputStream oos = new ObjectOutputStream(fos); // 2.
oos.writeObject(MYOBJECT); // 3.
oos.close(); // 4.
``` 
 
 #### Laden
 Um das gespeicherte Objekt wieder als solches im Code zur Verfügung zu haben, muss es aus der Datei geladen werden: <br>
 1. Ein `FileInputStream`-Objekt wird, mit dem relevanten Pfad als Argument, erstellt. 
 > Bei diesem Schritt kann eine `FileNotFoundException` auftreten!
 2. Über einen `ObjectInputStream`, welcher den oben initialisierten `FileInputStream` als Argument kriegt, kann das Objekt mittels der Methode 
 `readObject()` gelesen werden.
 3. Da es vom Typ Object ist, muss es gegebenenfalls gecastet werden.
 
 Es folgt ein Beispiel, in welchem das gelesene Objekt als Initialisierungswert für ein selbst erstelltes Objekt(*MyOwnObject*) genutzt werden soll:
```java
FileInputStream fis = new FileInputStream(PATH); // 1. (gegebenfalls try-catch-Block nötig)
MyOwnObject loaded = (MyOwnObject) new ObjectInputStream(fis).readObject(); // 2. & 3.
``` 
> **Weitere Informationen**:  <br>
http://docs.oracle.com/javase/7/docs/api/java/io/FileInputStream.html <br>
https://docs.oracle.com/javase/7/docs/api/java/io/FileOutputStream.html <br>
https://docs.oracle.com/javase/7/docs/api/java/io/ObjectInputStream.html <br>
https://docs.oracle.com/javase/7/docs/api/java/io/ObjectOutputStream.html


### JavalinValidation
*JavalinValidation* ist ein in Javalin integrierter Validator, um empfangene Parmeter zu validieren. So müssen 
bestimmte Überprüfungen nicht selber implementiert werden. Die *JavalinValidator* Klasse kann laut Javalin-Website 
auch erweitert werden, im Fall dieser Anwendung war dies jedoch nicht notwendig.
Der Einsatz des Validators ist sinnvoll, um zu überprüfen, ob erhaltene Parameter den Eigenschaften entsprichen, welche benötigt 
werden um fortzufahren. Ist dies nicht der Fall, wird die HTTP-Request mit einem *400 - Bad Request* beantwortet und nichts weiter ausgeführt. 
Somit kann das Ausführen von Code mit ungewollten Parameter oder Argumenten verhindert werden.
 <br> Folgende zwei Arten von Validation wurden in diesem Projekt benutzt:
1. Über die Methode `JavalinValidation.validate()`:
    <br> Diese Methode nimmt einen `String` entgegen, welcher auf verschiedene Eigenschaften überprüft werden kann. Beispielsweise soll lediglich fortgefahren werden, wenn dieser String weder `null` noch leer ist.
    <br> 
    ```java
   JavalinValidation.validate((ctx.formParam("meinparameter"))).notNullOrEmpty();
   ```
    Ebenfalls existieren `.asBoolean()`, `.asInt()`, `.asFloat()` etc. <br> 
    Es gilt : immer wenn die Bedingung nicht erfüllt wird, wird mit *400 - Bad Request* geantwortet.

2. Über folgende Syntax : `ctx.queryParam("meinparameter" , XXX.class).get()` <br>
    Somit kann direkt bei der Initialisierung einer Variable auch der Wert, welcher zugewiesen werden soll, validiert werden.:
    <br>
    ```java
   int meinInteger = ctx.queryParam("number" , Integer.class).get();
    ```
     In diesem Beispiel wird, **sofern der Parameter in einen Integer übersetzt werden kann**, dem `int` dieser Wert zugewiesen. Ansonsten liefere diese Zeile einen *Bad Request*. <br>
     Dies ist auch mit anderen `.class`'s möglich.
     
    <br>
    
    Ein weiteres integriertes Feature der JavalinValidation ist die `check()`-Methode. Sie kommt vor der 
    `get()`-Methode und erwartet eine Bedingung in Form eines Lambda-Ausdrucks. So könnte in unserem obigen Beispiel nicht
    nur validiert werden, dass es sich um einen Integer handelt, sondern auch jede andere denkbare Bedingung.
    Beispielsweise, ob der Integer kleiner als `100` ist. <br>
    
    ```java
    int meinInteger = ctx.queryParam("number" , Integer.class).check(i -> i < 100).get();
    ```
> **Weitere Informationen**: https://javalin.io/documentation#handler-groups

### Session-Handling

In diesem Projekt wurde eine Speicherung von Sessions im lokalen Dateisystem gewählt. Eine Speicherung in einer Datenbank wäre aber ebenfalls möglich gewesen.
Nachdem die beiden nötigen Methoden der [Anleitung auf der Javalin-Website](https://javalin.io/tutorials/jetty-session-handling-java) implementiert wurden, 
können in der Anwendung Sessions genutzt werden. <br>
Beim Starten des Servers muss die Methode `sessionHandler()` aufgerufen werden. Als Argument bekommt diese Methode eine Methodenreferenz zur `fileSessonHandler()`-
Methode, die nach Anleitung implementiert wurde.
    
 ```java
Javalin app = Javalin.create().sessionHandler(Sessions::fileSessionHandler) ...
 ```
In diesem Fall befindet sich die Methode `fileSessionHandler()` in der Klasse `Sessions`. 
Gewünschte Session-Attribute können mit `ctx.sessionAttribute(keyString, valueString);` gesetzt werden. Somit wird das zweite Argument(valueString) 
mit dem ersten Argument(keyString) als Key gespeichert. <br>
Wenn benötigt, können die gesetzten Attribute einfach über den zugewiesenen Key abgefragt werden : `ctx.sessionAttribute("keyString");`


>**Weitere Informationen**: https://javalin.io/tutorials/jetty-session-handling-java


## Quellennachweis

### 1. Allgemeine Quellen

Für allgemeine Information über JavaScript, HTML, CSS, Bootstrap, Java oder Javalin wurden die folgenden Quellen genutzt:

**Java**: <br> https://docs.oracle.com/en/java/javase/11/docs/api/index.html <br> Java ist auch eine Insel - Christian Ullenboom <br>

**Javalin**: <br> https://git.thm.de/dhzb87/JbX <br>
https://javalin.io/documentation <br>
https://javalin.io/tutorials/ <br>

**HTML/CSS**: <br> https://www.w3schools.com <br>

**Bootstrap**: <br> https://getbootstrap.com/docs/4.3/getting-started/introduction/ <br>

**JavaScript**: <br>
https://developer.mozilla.org/en-US/ <br>

### 2. Explizite Quellen

Im Folgenden werden die Quellen für Codeabschnitte, welche aus vorherigen Projekten, dem Internet oder anderen Quellen übernommen wurden aufgeführt. Sind Codeabschnitte nicht zu 100% übernommen, aber dennoch ähnlich, sind sie ebenfalls hier aufgeführt. <br>

| Datei | Anmerkung | Zeilen |
| --- | --- | --- |
| **[FahrtenbuchTest.java](src/test/java/FahrtenbuchTest.java)** | Um einen Ordner zu löschen, welcher nicht leer ist, wurde eine rekursive Lösung von https://www.baeldung.com/java-delete-directory übernommen. |  95-102 |
| **[Sessions.java](src/main/java/pack/Sessions.java)**  | Die `Sessions.java` wurde mithilfe des Tutorials der Javalin Website (https://javalin.io/tutorials/jetty-session-handling-java) implementiert. | komplette Datei |
| **[UserManagement.java](src/main/java/pack/UserManagement.java)**  | Für diese Datei wurde eine Datei namens `Authenticator.java` aus meinem Gruppenprojekt als Grundlage genommen, welche [hier](Authenticator.java) zu finden ist. | - |
| **[Coder.java](src/main/java/pack/Coder.java)**  | Nach der Suche nach einer in Java integrierte Kodierungsmöglichkeit wurde die Base64 Klasse gefunden:  https://docs.oracle.com/javase/8/docs/api/java/util/Base64.html | - |
| **[StoreableHandler.java](src/main/java/pack/StoreableHandler.java)** | Auf der Suche nach einer Möglichkeit Java `ArrayLists` in Dateien zu speichern wurde von folgender StackOverflow Antwort gebrauch gemacht: https://stackoverflow.com/a/16111791 | 9-14 , 1-35 | 
| **[MarkdownConverter.java](src/main/java/pack/MarkdownConverter.java)**  | Um die Tabelle in Markdown Syntax in eine Datei zu schreiben, wurde eine Methode benutzt, welche ich bereits für das Gruppenprojekt schrieb. Es handelt sich um die Methode `writeFile()`. [Originaldatei](FileWrite.java) |  67 - 77 | 
| **Hintergrundbild** | Quelle : [Shutterstock](https://www.shutterstock.com/image-photo/long-road-mountain-forest-1325761397). Das Hintergrundbild wurde nach dem Download mit GIMP bearbeitet. | 
| **Odos - Logo** | Das Logo, welches auf der Startseite (Login) zu sehen ist, wurde mithilfe von https://logomakr.com erstellt. |  - | 

<br>


## Skizzen-Entwürfe der grafischen Oberfläche - Erste Design-Ansätze


> **Oben links** - Einloggen und Registrieren | **Oben rechts** - Tankbelegeübersicht | **Unten links** - Fahrtenübersicht | **Unten rechts** - Fahrt hinzufügen 


![](Collage.jpg)


>  Die Entwürfe wurden auf https://pidoco.com/en erstellt. <br>
> **Bemerkung bezüglich des Umfangs auf den Entwürfen**: Ursprünglich wurde neben den Fahrten ebenfalls eine Verwaltung von Tankbelegen und Informationsübersicht angedacht. Nach Mitteilung des Bewertungskriteriums *LOC* mussten die Ansätze dieser Features jedoch wieder entfernt werden.


>**[Dieses Projekt auf THM-GITLAB](https://git.thm.de/lkis64/pis-klausurleistung)**
