// Dieser Code wurde im Teamprojekt MyLifeApp - TodoListe benutzt.
package pack;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

class FileWrite {

    static void writeFile(String filename, List<String> lst) throws IOException {
        FileWriter writer = new FileWriter(filename);
        lst.forEach(s -> {
            try {
                writer.write(s+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        writer.close();
    }
}
